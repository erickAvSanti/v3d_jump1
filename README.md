A Threejs 3D project with a custom collision system for testing purpose, you can use AWSD keys to move around with mouse pointer lock control inclusive. Jump with Space key.

You must have PHP cli.

Run local server after git clone with "php -S localhost:8181" and open your browser.

Here are screen samples: https://gitlab.com/erickAvSanti/v3d_jump1/-/tree/master/screenshots
