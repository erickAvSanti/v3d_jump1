var appgame = appgame || {};

appgame.Shooter = (
	function(){

		var _instance = null;  

		var _balls = [];
		var _ball_velocity = 50/1000;

		var _ball_instance = undefined;

		var _maxRadiusBallDistance = 300;

		var _ball_distance_camera = 2;

		var _dt = 20/1000;

		var _parent = undefined;//CameraControl

		var _aproxZero = 0.00001;
 

		var _interval_mousedown_id = -1;


		var _loader = new THREE.JSONLoader();

		var _elements = {}; 
		_elements["misil1"] = "./modelos/misil1.json";  

		var _mousedown = function(){
			if(_interval_mousedown_id==-1){
				_interval_mousedown_id = setInterval(_instance.listen, 100);
			}
     		
		};
		var _mouseup = function(){
			if(_interval_mousedown_id!=-1){
		     	clearInterval(_interval_mousedown_id);
		     	_interval_mousedown_id=-1;
		   	}
		};

		var _dispPos1 = new THREE.Vector3();
		var _dispPos2 = new THREE.Vector3();

		Shooter = function(_p){
			_parent = _p;
		};
		Shooter.prototype = {
			constructor:Shooter, 
			test:function(){
				console.log("hello from Shooter");
			},
			start:function(){  
				if(w_wgl.scene){/*
					var geometry = new THREE.SphereGeometry( 0.2, 8, 8 );
					var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
					material.opacity = 0.2;
					material.transparent = true;
					var sphere = new THREE.Mesh( geometry, material ); 
					_ball_instance = sphere;*/


					var _this = this;
					_loader.load( 
						_elements["misil1"], 
						function ( geometry, materials ) {  
							for(var _idx in materials){
								//materials[_idx].side = THREE.FrontSide; 
								materials[_idx].opacity = 1;
							}
							console.log("misil1",materials);
							_ball_instance = new THREE.Mesh( geometry, materials );  
						}
					);
				} 
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					/*$(w_ctouch).click(
						function(_evt){
							_this.listen(_evt);
						}
					);*/
					$(w_ctouch).mousedown(function(_evt){ 
						_mousedown();
					});
					$(w_ctouch).mouseup(function(_evt){ 
						_mouseup();
					});
					$(w_ctouch).mouseleave(function(_evt){ 
						_mouseup();
					});
					$(w_ctouch).mouseout(function(_evt){ 
						_mouseup();
					});
				} 
				
			}, 
			listen:function(evt){   
				if(w_wgl.camera){
					var _dir = w_wgl.camera.getWorldDirection();
					var _pos = w_wgl.camera.position;
					var _ball = undefined;
					var _ball_find = false;
					for(var _idx in _balls){
						_ball = _balls[_idx];
						if(!_ball.visible){
							_ball_find = true;
							break;
						}
					}

					if(!_ball_find){
						_ball = _ball_instance.clone(); 
						_balls.push(_ball);
						w_wgl.scene.add(_ball);
					} 
					_ball._cin1 = {};
					_ball._cin1.v0 = 700/1000;
					_ball._cin1.v0_def = 700/1000;
					_ball._cin1.vf = 0;
					_ball._cin1.vf_def = 0;
					_ball._cin1.acc = 900/1000;
					_ball.visible = true;
					_ball.rot = {};
					_ball.rot.def = 100/1000;
					_ball.rot.curr = 0;
					_ball.rot.inc = 0/1000;
					_ball.rot.inc_inc = 5/1000;
					_dispPos1 = _dir.clone().normalize().multiplyScalar(_ball_distance_camera);
					//_ball.position.copy(_pos).add(_dispPos1);
					_ball.position.copy(_pos);
					_dispPos2 = _ball.position.clone().add(_dispPos1);
					_ball.lookAt(_dispPos2);
					_ball.position.copy(_pos).add(_dispPos1);
					_ball._lib_last_position = new THREE.Vector3();
					_ball._lib_last_position.copy(_ball.position);
					_ball._dir = _dir.clone().normalize(); 
					if(_ball._lib_block_objs)delete _ball._lib_block_objs;
					_ball._lib_block_objs = {};
					_ball._lib_blocks_ray_casted = false;

				}
			}, 
			render:function(){ 
				for(var _idx in _balls){
					var _ball = _balls[_idx];
					if(_ball.visible){
						_ball._lib_last_position.copy(_ball.position);
						_ball.position.x +=_ball._dir.x*_ball._cin1.vf;
						_ball.position.y +=_ball._dir.y*_ball._cin1.vf;
						_ball.position.z +=_ball._dir.z*_ball._cin1.vf;
						_ball._cin1.vf = _ball._cin1.v0 + _ball._cin1.acc*_dt;
						_ball._cin1.v0 = _ball._cin1.vf;

						_ball.rot.curr += _ball.rot.inc;
						_ball.rotation.z = _ball.rot.curr;
						_ball.rot.inc += _ball.rot.inc_inc;

						if(_ball.position.distanceTo(w_wgl.camera.position)>_maxRadiusBallDistance || _ball.position.y<-200){
							_ball.visible = false;
						}
					} 
					if(_ball.visible){
						this.checkCollisions(_ball); 
					}else{
						continue;
					}
				}
			}, 
			checkCollisions:function(_ball){ 
				//this.checkCollisionsWithBlocks(_ball);
				if(!_ball._lib_blocks_ray_casted)this.rayBallIntersectBlocks(_ball);
				this.checkCollisionsBallWithBlocks(_ball);
			}, 
			rayBallIntersectBlocks:function(_ball){
				if(!_ball.visible)return; 
				if(_lib_spheres_blocks){
					for(var _idx in _lib_spheres_blocks){
						if(!_ball.visible)break;
						var _sphere = _lib_spheres_blocks[_idx];
						var _res = this.line3IntersectSphere(
								_ball.position,
								_ball._dir,
								_sphere.center,
								_sphere.radius
							);
						if(_res){
							if(_sphere._lib_objs){
								for(var _idy in _sphere._lib_objs){
									if(!_ball.visible)break;
									var _block = _sphere._lib_objs[_idy]; 
									var _res2 = this.line3IntersectSphere(
											_ball.position,
											_ball._dir,
											_block.position,
											_block._lib_radius
										);
									if(_res2){
										if(!_ball._lib_block_objs[_block.uuid]){
											_ball._lib_block_objs[_block.uuid] = {mesh:_block,extra:_res2}; 
										} 
									} 
								}
							} 
						}
					}
					_ball._lib_blocks_ray_casted = true;
				}
			},
			checkCollisionsBallWithBlocks:function(_ball){
				if(!_ball._lib_block_objs)return; 
				for(var idx in _ball._lib_block_objs){
					if(!_ball.visible)break;
					var _remove_block_data = false;
					var _block_data = _ball._lib_block_objs[idx];
					var _block = _block_data.mesh;
					if(!_ball._lib_last_position)continue;
					var _extra = _block_data.extra;
					if(_extra instanceof THREE.Line3){
						if(!_block_data.checked){
							var _last_block_position = _block.position.clone();
							var _arr_desfase = [-0.3,0.3]; 
							var _point = null;
							for(var _idxdes in _arr_desfase){ 
								if(!_ball.visible)break;
								var _desf = _arr_desfase[_idxdes];
								_last_block_position.y +=_desf;
								_point = this.line3IntersectPlane(
										_extra.start,
										_ball._dir,
										_block._lib_plane.normal,
										_block._lib_plane.constant+_desf,
										_last_block_position 
									);
							
								if(_point instanceof THREE.Vector3){
									if(_point.distanceTo(_block.position)<_block._lib_radius){ 
										var _triangle_contains_point = false;
										for(var _idz in _block._lib_triangles){ 
											var _triangle = _block._lib_triangles[_idz].clone();
											_triangle.a.y +=_desf;
											_triangle.b.y +=_desf;
											_triangle.c.y +=_desf;
											if(_triangle.containsPoint(_point)){ 
												_triangle_contains_point = true;
												break;  
											}
										}
										if(_triangle_contains_point){ 
											if(!_block_data.point)_block_data.point = [];
											_block_data.point.push(_point);
											var _v1 = _point.clone().sub(_ball.position);
											var _v2 = _point.clone().sub(_ball._lib_last_position);
											var _dot = _v1.dot(_v2); 
											if(_dot<0){ 
												_ball.visible = false;
											} 
										}else{
											_point = null;
										}
									}else{ 
									} 
								}else{ 
								} 
								_last_block_position.copy(_block.position);
							}
							if(!_point){
								_remove_block_data = true;
							}

							_block_data.checked = true;
						}else if(_block_data.point){ 
							for(var _idxp in _block_data.point){
								if(!_ball.visible)break;
								_point = _block_data.point[_idxp];
								if(_point instanceof THREE.Vector3){
									var _v1 = _point.clone().sub(_ball.position);
									var _v2 = _point.clone().sub(_ball._lib_last_position);
									var _dot = _v1.dot(_v2); 
									if(_dot<0){ 
										_ball.visible = false;
									} 
								}
							}
						}
					}else{
						console.log(_extra);
						_remove_block_data = true;
					}
					if(_remove_block_data){
						delete _ball._lib_block_objs[idx];
					}
				}
			},
			checkCollisionsWithBlocks:function(_ball){
				if(!_ball.visible)return;
				if(_lib_spheres_blocks){
					for(var _idx in _lib_spheres_blocks){  
						if(!_ball.visible)break;
						var _sphere = _lib_spheres_blocks[_idx];
						if(this.sphereMeshCollisionSphere(_sphere,_ball)){
							if(_sphere._lib_objs){
								for(var _idy in _sphere._lib_objs){
									if(!_ball.visible)break;
									var _block = _sphere._lib_objs[_idy];  
									if(this.meshMeshCollisionSphere(_block,_ball)){
										for(var _idz in _block._lib_triangles){
											if(!_ball.visible)break;
											var _triangle = _block._lib_triangles[_idz]; 
											if(_triangle.boundingSphere.middle.distanceTo(_ball.position)<=(_triangle.boundingSphere.radius + _ball.geometry.boundingSphere.radius)){ 
												for(var _idn in _triangle._lib_lines3){
													if(!_ball.visible)break;
													var _line3 = _triangle._lib_lines3[_idn]; 
													if(this.line3SphereIntersecction(_line3,_ball)){
														_ball.visible = false;
													}
												} 
												if(_ball.visible){ 
													var _p1 = _triangle._lib_plane.projectPoint(_ball.position);
													if(_triangle.containsPoint(_p1)){
														var _d1 = _triangle._lib_plane.distanceToPoint(_ball.position);
														if(_d1<_ball.geometry.boundingSphere.radius){
															_ball.visible = false;
														}else{ 
															var _vec = _ball.position.clone().sub(_triangle.boundingSphere.middle);
															var _sign = Math.sign(_vec.dot(_triangle._lib_plane.normal));
															if(!_ball._lib_last_orientation){
																_ball._lib_last_orientation = _sign;
																_ball._lib_last_block_triangle_plane_uuid = _triangle._lib_plane.uuid;
															} 
															if(_sign!=_ball._lib_last_orientation){
																if(_ball._lib_last_block_triangle_plane_uuid == _triangle._lib_plane.uuid){
																	_ball.visible = false;
																}
															}else{
																_ball._lib_last_block_triangle_plane_uuid = _triangle._lib_plane.uuid;
															}
														}	
													} 
												}	
											}
										} 
									}	
								}
							}
						}
					}
				}
			}, 
			line3SphereIntersecction:function(line3,obj){
				var _r2 = 0;
				var _o = line3.start;
				var _c = 0;
				var _l = line3._lib_vunit;
				if(obj instanceof THREE.Sphere){
					_r2 = Math.pow(sphere.radius,2);
					_c = sphere.center.clone();
				}else if(obj instanceof THREE.Mesh){
					_r2 = Math.pow(obj.geometry.boundingSphere.radius,2);
					_c = obj.geometry.boundingSphere.center.clone().add(obj.position);
				}else{
					return false;
				}

				var _oc1 = _c.clone().sub(_o);
				_oc1 = Math.pow(_oc1.x,2)+Math.pow(_oc1.y,2)+Math.pow(_oc1.z,2);

				var _oc2 = _c.clone().sub(_o);
				_oc2 = _oc2.dot(_l);
				_oc2 = Math.pow(_oc2,2);

				var _result = _oc2 - _oc1 + _r2;
				if(_result>=0)return true;
			},
			sphereMeshCollisionSphere:function(sphere,mesh){
				return mesh.geometry.boundingSphere && sphere.center.distanceTo(mesh.position)<=(sphere.radius+mesh.geometry.boundingSphere.radius);
			},
			meshMeshCollisionSphere:function(mesh1,mesh2){ 
				if(
					mesh1.geometry.boundingSphere && 
					mesh2.geometry.boundingSphere 
				){
					var _sum = mesh1.geometry.boundingSphere.radius + mesh2.geometry.boundingSphere.radius;
					return mesh1.position.distanceTo(mesh2.position) <= _sum;
				}
				return false;
			},
			line3IntersectPlane:function(l_point,l_dir,p_normal,p_constant,p_point){
				var _dot1 = l_dir.dot(p_normal);
				var _dot2 = p_point.clone().sub(l_point).dot(p_normal);
				if(_dot1<_aproxZero && _dot1>-_aproxZero){//casi cero.
					if(_dot2<_aproxZero && _dot2>-_aproxZero){//casi cero.
						// linea contenida en el plano
						return 1;
					}else{
						// linea y plano no se intersectan.
						return 0;
					}
				}else{
					var _d = _dot2/_dot1;
					return l_dir.clone().multiplyScalar(_d).add(l_point);
				}
			},
			line3IntersectSphere:function(l_point,l_dir,s_center,s_radius){ 
				var _oc = l_point.clone().sub(s_center);
				var _disc = Math.pow(_oc.dot(l_dir),2) - _oc.lengthSq() + Math.pow(s_radius,2);
				//x = o + dl;
				var _dot1 = _oc.dot(l_dir);
				if(_disc<0){
					//no hay soluciÃ³n
					return null;
				}else if(_disc==0){
					//una soluciÃ³n
					return l_dir.clone().multiplyScalar(-_dot1).add(l_point);
				}else{
					var _disc_sqrt = Math.sqrt(_disc);
					var _d1 = -_dot1 + _disc_sqrt;
					var _d2 = -_dot1 - _disc_sqrt;
					return new THREE.Line3(
						l_dir.clone().multiplyScalar(_d2).add(l_point),
						l_dir.clone().multiplyScalar(_d1).add(l_point)
					);
				}
			},
			resize:function(){ 
			} 
		};

		return {
			instance:function(_p){
				if(!_instance){
					_instance = new Shooter(_p);
					return _instance;
				}
				return null;
			}
		};
	}
)();