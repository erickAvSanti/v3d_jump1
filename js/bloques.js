var appgame = appgame || {};

appgame.Bloques = (
	function(){

		var _instance = null;  

		var _loader = new THREE.JSONLoader(); 

		var _zExtraPos = 12;

		var _models = {
			"m1":"./modelos/bloque.json",
			"m2":"./modelos/bloque2.json",
			"m3":"./modelos/bloque3.json"
		};

		window._lib_bm2 = [];
		window._lib_bm3 = [];

		window._lib_spheres_blocks = [/*for collisions*/
			new THREE.Sphere(new THREE.Vector3(20.8113,0.0000,-63.3389),45),
			new THREE.Sphere(new THREE.Vector3(66.3455,0.0000,-47.7450),45),
			new THREE.Sphere(new THREE.Vector3(38.9002,0.0000,-1.5871),45),
			new THREE.Sphere(new THREE.Vector3(65.0979,0.0000,52.6797),45),
			new THREE.Sphere(new THREE.Vector3(22.6825,0.0000,67.0260),45),
			new THREE.Sphere(new THREE.Vector3(-24.0992,0.0000,41.4521),45),
			new THREE.Sphere(new THREE.Vector3(-25.9704,0.0000,-27.7849),45),
			new THREE.Sphere(new THREE.Vector3(-52.7919,0.0000,-0.9633),45),
			new THREE.Sphere(new THREE.Vector3(-1.0202,0.0000,-3.4584),45),
		];

		var _triangles_per_block = [
			new THREE.Triangle( new THREE.Vector3(2.42044,-0.0,-2.42044),new THREE.Vector3(0.0,-0.0,-3.42302),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(0.0,-0.0,-3.42302),new THREE.Vector3(-2.42044,-0.0,-2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-2.42044,-0.0,-2.42044),new THREE.Vector3(-3.42302,-0.0,0.0),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-3.42302,-0.0,0.0),new THREE.Vector3(-2.42044,-0.0,2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-2.42044,-0.0,2.42044),new THREE.Vector3(-0.0,-0.0,3.42302),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-0.0,-0.0,3.42302),new THREE.Vector3(2.42044,-0.0,2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(2.42044,-0.0,2.42044),new THREE.Vector3(3.42302,-0.0,0.0),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(3.42302,-0.0,0.0),new THREE.Vector3(2.42044,-0.0,-2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
		];

		var _radius_per_block = 3.7;

		window._pos_m2 = [
			new THREE.Vector3(46.3914,5.0850,-22.5278),
			new THREE.Vector3(37.7390,3.7434,-18.5649),
			new THREE.Vector3(29.0859,2.4017,-14.6015),
			new THREE.Vector3(20.4328,1.0600,-10.6381),
			new THREE.Vector3(11.7797,-0.2817,-6.6747),
			new THREE.Vector3(-5.0299,16.9701,-50.7093),
			new THREE.Vector3(-1.5693,16.0895,-58.9378),
			new THREE.Vector3(3.5800,15.2093,-66.2266),
			new THREE.Vector3(10.1837,14.3287,-72.2342),
			new THREE.Vector3(17.9252,13.4484,-76.6774),
			new THREE.Vector3(26.4473,12.5672,-79.3436),
			new THREE.Vector3(35.3419,11.6865,-80.1122),
			new THREE.Vector3(44.1918,10.8059,-78.9466),
			new THREE.Vector3(52.5822,9.9253,-75.9003),
			new THREE.Vector3(60.1188,9.0447,-71.1174),
			new THREE.Vector3(66.4473,8.1641,-64.8219),
			new THREE.Vector3(71.2698,7.2835,-57.3102),
			new THREE.Vector3(74.3598,6.4029,-48.9356),
			new THREE.Vector3(10.3881,-7.2861,-61.4611),
			new THREE.Vector3(1.8905,-6.5080,-60.6445),
			new THREE.Vector3(-6.0706,-5.7298,-57.5631),
			new THREE.Vector3(-12.9040,-4.9514,-52.4458),
			new THREE.Vector3(-18.1022,-4.1731,-45.6741),
			new THREE.Vector3(-21.2791,-3.3949,-37.7505),
			new THREE.Vector3(-22.1987,-2.6166,-29.2633),
			new THREE.Vector3(-20.7927,-1.8383,-20.8431),
			new THREE.Vector3(-17.1656,-1.0600,-13.1152),
			new THREE.Vector3(-11.5867,-0.2817,-6.6535),
			new THREE.Vector3(-25.0713,-1.9419,-0.1101),
			new THREE.Vector3(-33.6839,-3.6543,-0.0734),
			new THREE.Vector3(-42.2967,-5.3667,-0.0367),
			new THREE.Vector3(-50.9094,-7.0791,-0.0000),
			new THREE.Vector3(45.6285,5.0851,22.1048),
			new THREE.Vector3(37.2299,3.7434,18.2826),
			new THREE.Vector3(28.8313,2.4017,14.4603),
			new THREE.Vector3(20.4328,1.0600,10.6381),
			new THREE.Vector3(12.0342,-0.2817,6.8159),
			new THREE.Vector3(-5.2093,16.9703,53.7925),
			new THREE.Vector3(-1.6320,16.0899,61.9961),
			new THREE.Vector3(3.6381,15.2084,69.2332),
			new THREE.Vector3(10.3333,14.3283,75.1691),
			new THREE.Vector3(18.1520,13.4481,79.5207),
			new THREE.Vector3(26.7290,12.5672,82.0821),
			new THREE.Vector3(35.6548,11.6867,82.7354),
			new THREE.Vector3(44.5114,10.8061,81.4500),
			new THREE.Vector3(52.8842,9.9254,78.2838),
			new THREE.Vector3(60.3768,9.0447,73.3883),
			new THREE.Vector3(66.6378,8.1641,66.9931),
			new THREE.Vector3(71.3730,7.2835,59.3984),
			new THREE.Vector3(74.3598,6.4029,50.9615),
			new THREE.Vector3(10.9124,-7.2863,61.1446),
			new THREE.Vector3(2.3194,-6.5078,60.5988),
			new THREE.Vector3(-5.7882,-5.7296,57.7031),
			new THREE.Vector3(-12.7808,-4.9514,52.6811),
			new THREE.Vector3(-18.1141,-4.1731,45.9218),
			new THREE.Vector3(-21.3732,-3.3949,37.9531),
			new THREE.Vector3(-22.3046,-2.6166,29.3938),
			new THREE.Vector3(-20.8357,-1.8383,20.9103),
			new THREE.Vector3(-17.0808,-1.0600,13.1625),
			new THREE.Vector3(-11.3321,-0.2817,6.7531),
			new THREE.Vector3(-16.4586,-0.2295,-0.1468),
			new THREE.Vector3(-21.8135,17.9723,-27.3615),
			new THREE.Vector3(-22.0192,17.6906,-40.5611),
			new THREE.Vector3(-22.0192,17.6906,-13.5799),
			new THREE.Vector3(-33.6059,17.6906,-34.0151),
			new THREE.Vector3(-33.3513,17.6906,-20.6084),
			new THREE.Vector3(-9.9849,17.6906,-20.5456),
			new THREE.Vector3(-10.2395,17.6906,-34.0362),
			new THREE.Vector3(-68.3325,-6.7974,-0.0722),
			new THREE.Vector3(-68.5381,-7.0791,-13.2717),
			new THREE.Vector3(-68.5381,-7.0791,13.7094),
			new THREE.Vector3(-80.1248,-7.0791,-6.7257),
			new THREE.Vector3(-79.8703,-7.0791,6.6809),
			new THREE.Vector3(-56.5039,-7.0791,6.7437),
			new THREE.Vector3(-56.7585,-7.0791,-6.7469),
			new THREE.Vector3(-10.2395,17.6906,24.0077),
			new THREE.Vector3(-9.9849,17.6906,37.4983),
			new THREE.Vector3(-33.3513,17.6906,37.4355),
			new THREE.Vector3(-33.6059,17.6906,24.0289),
			new THREE.Vector3(-22.0192,17.6906,44.4641),
			new THREE.Vector3(-22.0192,17.6906,17.4829),
			new THREE.Vector3(-21.8135,17.9723,30.6824),
			new THREE.Vector3(86.1395,6.4029,-42.4107),
			new THREE.Vector3(86.3940,6.4029,-28.9201),
			new THREE.Vector3(63.0277,6.4029,-28.9829),
			new THREE.Vector3(62.7731,6.4029,-42.3896),
			new THREE.Vector3(74.3598,6.4029,-21.9544),
			new THREE.Vector3(-6.6367,17.8508,-41.9282),
			new THREE.Vector3(74.5654,6.6846,-35.7360),
			new THREE.Vector3(39.9845,-7.8335,-49.3807),
			new THREE.Vector3(39.7789,-8.1153,-62.5802),
			new THREE.Vector3(39.7789,-8.1153,-35.5991),
			new THREE.Vector3(28.1922,-8.1153,-56.0342),
			new THREE.Vector3(28.4467,-8.1153,-42.6276),
			new THREE.Vector3(51.8131,-8.1153,-42.5648),
			new THREE.Vector3(51.5586,-8.1153,-56.0554),
			new THREE.Vector3(51.5586,-8.1153,42.7060),
			new THREE.Vector3(51.8131,-8.1153,56.1965),
			new THREE.Vector3(28.4467,-8.1153,56.1338),
			new THREE.Vector3(28.1922,-8.1153,42.7271),
			new THREE.Vector3(39.7789,-8.1153,63.1623),
			new THREE.Vector3(39.7789,-8.1153,36.1811),
			new THREE.Vector3(39.9845,-7.8335,49.3807),
			new THREE.Vector3(74.5654,6.6846,37.1799),
			new THREE.Vector3(74.3598,6.4029,23.9804),
			new THREE.Vector3(-6.9351,17.8508,45.0108),
			new THREE.Vector3(62.7731,6.4029,30.5264),
			new THREE.Vector3(63.0277,6.4029,43.9330),
			new THREE.Vector3(86.3940,6.4029,43.9958),
			new THREE.Vector3(19.3189,-8.0641,59.2908),
			new THREE.Vector3(0.0000,-0.2817,13.7816),
			new THREE.Vector3(54.0263,6.4267,25.9266),
			new THREE.Vector3(0.0000,-0.2817,-13.1995),
			new THREE.Vector3(86.1395,6.4029,30.5052),
			new THREE.Vector3(55.0451,6.4269,-26.4917),
			new THREE.Vector3(18.7914,-8.0647,-59.9563),
			new THREE.Vector3(0.0000,0.0000,-0.0000),
		];
 
		window._pos_m3 = [
			new THREE.Vector3(-29.214447021484375,17.690576553344727,-27.459936141967773),
			new THREE.Vector3(-25.46117401123047,17.69057846069336,-33.86648178100586),
			new THREE.Vector3(-18.03630828857422,17.69057846069336,-33.81932830810547),
			new THREE.Vector3(-14.364707946777344,17.690576553344727,-27.365625381469727),
			new THREE.Vector3(-18.11798095703125,17.690576553344727,-20.959075927734375),
			new THREE.Vector3(-25.5428466796875,17.690576553344727,-21.006237030029297),
			new THREE.Vector3(-75.73342895507812,-7.079076766967773,-0.17061996459960938),
			new THREE.Vector3(-71.98015594482422,-7.079074859619141,-6.577165603637695),
			new THREE.Vector3(-64.55529022216797,-7.079074859619141,-6.530010223388672),
			new THREE.Vector3(-60.883689880371094,-7.079076766967773,-0.0763092041015625),
			new THREE.Vector3(-64.636962890625,-7.079076766967773,6.330240249633789),
			new THREE.Vector3(-72.06182861328125,-7.079076766967773,6.283079147338867),
			new THREE.Vector3(-25.5428466796875,17.690576553344727,37.037715911865234),
			new THREE.Vector3(-18.11798095703125,17.690576553344727,37.084877014160156),
			new THREE.Vector3(-14.364707946777344,17.690576553344727,30.678327560424805),
			new THREE.Vector3(-18.03630828857422,17.69057846069336,24.224626541137695),
			new THREE.Vector3(-25.46117401123047,17.69057846069336,24.177471160888672),
			new THREE.Vector3(-29.214447021484375,17.690576553344727,30.584016799926758),
			new THREE.Vector3(70.83609771728516,6.402912616729736,-29.3807373046875),
			new THREE.Vector3(78.2609634399414,6.4029130935668945,-29.333576202392578),
			new THREE.Vector3(82.01423645019531,6.4029130935668945,-35.7401237487793),
			new THREE.Vector3(78.34263610839844,6.402913570404053,-42.193824768066406),
			new THREE.Vector3(70.91777038574219,6.402913570404053,-42.24098205566406),
			new THREE.Vector3(67.16449737548828,6.402912616729736,-35.834434509277344),
			new THREE.Vector3(32.58358383178711,-8.115257263183594,-49.479103088378906),
			new THREE.Vector3(36.336856842041016,-8.115256309509277,-55.88564682006836),
			new THREE.Vector3(43.761722564697266,-8.115256309509277,-55.83849334716797),
			new THREE.Vector3(47.43332290649414,-8.115256309509277,-49.38479232788086),
			new THREE.Vector3(43.680049896240234,-8.115257263183594,-42.97824478149414),
			new THREE.Vector3(36.255184173583984,-8.115257263183594,-43.02540588378906),
			new THREE.Vector3(36.255184173583984,-8.115257263183594,55.73595428466797),
			new THREE.Vector3(43.680049896240234,-8.115257263183594,55.78311538696289),
			new THREE.Vector3(47.43332290649414,-8.115256309509277,49.37656784057617),
			new THREE.Vector3(43.761722564697266,-8.115256309509277,42.92286682128906),
			new THREE.Vector3(36.336856842041016,-8.115256309509277,42.87571334838867),
			new THREE.Vector3(32.58358383178711,-8.115257263183594,49.282257080078125),
			new THREE.Vector3(67.16449737548828,6.402912616729736,37.08147430419922),
			new THREE.Vector3(70.91777038574219,6.402913570404053,30.674928665161133),
			new THREE.Vector3(78.34263610839844,6.402913570404053,30.722084045410156),
			new THREE.Vector3(82.01423645019531,6.4029130935668945,37.175785064697266),
			new THREE.Vector3(78.2609634399414,6.4029130935668945,43.582332611083984),
			new THREE.Vector3(70.83609771728516,6.402912616729736,43.53517150878906),
			new THREE.Vector3(-3.7643394470214844,-0.2817131578922272,6.355274677276611),
			new THREE.Vector3(3.6605279445648193,-0.28171271085739136,6.4024338722229),
			new THREE.Vector3(7.413800239562988,-0.28171247243881226,-0.004111289978027344),
			new THREE.Vector3(3.742206335067749,-0.28171226382255554,-6.457812309265137),
			new THREE.Vector3(-3.682661533355713,-0.2817119061946869,-6.504968166351318),
			new THREE.Vector3(-7.4359331130981445,-0.2817131578922272,-0.09842395782470703),
		];

		Bloques = function(){
		};
		Bloques.prototype = {
			constructor:Bloques, 
			test:function(){
				console.log("hello from Bloques");
			},
			start:function(){  
				var _this = this;
				_loader.load( 
					_models["m2"], 
					function ( geometry, materials ) {  
						for(var _idx in materials){
							materials[_idx].side = THREE.FrontSide;
						}
						var object = new THREE.Mesh( geometry, materials ); 
						for(var _idx in _pos_m2){
							object = object.clone(); 
							object.position.copy(_pos_m2[_idx]); 
							object.position.y+=_zExtraPos;
							_this.setCollisionLimits(object);
							_lib_bm2.push(object);
							w_wgl.scene.add( object ); 
						} 
					}
				);
				_loader.load( 
					_models["m3"], 
					function ( geometry, materials ) { 
						for(var _idx in materials){
							materials[_idx].side = THREE.FrontSide;
						} 
						var object = new THREE.Mesh( geometry, materials ); 
						for(var _idx in _pos_m3){
							object = object.clone();
							object.position.copy(_pos_m3[_idx]); 
							object.position.y+=_zExtraPos;
							_this.setCollisionLimits(object);
							_lib_bm3.push(object);
							w_wgl.scene.add( object ); 
						} 
					}
				);

				this.setListener();
			},
			setCollisionLimits:function(block){
				block._lib_triangles = [];
				var _block_normal = new THREE.Vector3(0,1,0);
				block._lib_plane = new THREE.Plane(_block_normal,_block_normal.dot(block.position));
				block._lib_radius = _radius_per_block;
				for(var _idx in _triangles_per_block){
					block._lib_triangles.push(_triangles_per_block[_idx].clone());
				}
				for(var _idx in block._lib_triangles){
					var _triangle = block._lib_triangles[_idx];
					_triangle.a.add(block.position);
					_triangle.b.add(block.position);
					_triangle.c.add(block.position);
					_triangle._lib_lines3 = [
						new THREE.Line3(_triangle.a,_triangle.b),
						new THREE.Line3(_triangle.b,_triangle.c),
						new THREE.Line3(_triangle.c,_triangle.a)
					];
					//el primer segmento de cada triÃƒÂ¡ngulo es el que estÃƒÂ¡ al borde del bloque.
					_triangle._lib_lines3[0]._lib_vunit = _triangle.b.clone().sub(_triangle.a).normalize();
					_triangle._lib_lines3[1]._lib_vunit = _triangle.c.clone().sub(_triangle.b).normalize();
					_triangle._lib_lines3[2]._lib_vunit = _triangle.a.clone().sub(_triangle.c).normalize();

					var _normal = _triangle._lib_lines3[0]._lib_vunit.clone().cross(_triangle._lib_lines3[1]._lib_vunit).normalize();

					_triangle._lib_plane = new THREE.Plane(
							_normal,
							_normal.dot(_triangle.a)
						); 

					_triangle._lib_plane.uuid = THREE.Math.generateUUID();

					var _middle = this.triangleMiddlePoint(_triangle);
					_triangle.boundingSphere = {
						middle:_middle,
						radius:this.triangleBoundingSphere(_triangle,_middle)
					}; 
				}
				for(var _idx in _lib_spheres_blocks){
					var _sphere = _lib_spheres_blocks[_idx];
					if(!block.geometry.boundingSphere)block.geometry.computeBoundingSphere();
					if(this.sphereMeshCollisionSphere(_sphere,block)){
						if(!_sphere._lib_objs)_sphere._lib_objs = [];
						_sphere._lib_objs.push(block);
					}
				}
			},
			sphereMeshCollisionSphere:function(sphere,mesh){ 
				return mesh.geometry.boundingSphere && sphere.center.distanceTo(mesh.position)<=(sphere.radius+mesh.geometry.boundingSphere.radius);
			},
			triangleMiddlePoint:function(triangle){
				return triangle.a.clone().add(triangle.b).add(triangle.c).multiplyScalar(1/3);
			},
			triangleBoundingSphere:function(triangle,middle){
				var _da = middle.distanceTo(triangle.a);
				var _db = middle.distanceTo(triangle.b);
				var _dc = middle.distanceTo(triangle.c);
				return Math.max(_da,_db,_dc);
			},
			setListener:function(){
			}, 
			listen:function(evt){ 
			}, 
			render:function(){ 
			}, 
			resize:function(){ 
			} 
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Bloques();
					return _instance;
				}
				return null;
			}
		};
	}
)();