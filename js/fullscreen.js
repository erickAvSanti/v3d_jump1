var appgame = appgame || {};

appgame.FullScreen = (
	function(){

		var _instance = null; 

		var _btn_maximize_src = "./imgs/boton_maximize.png";
		var _btn_minimize_src = "./imgs/boton_minimize.png";

		var _btn_maximize = null;
		var _btn_minimize = null;  

		var _width = 90;
		var _height = 90;

		var _radius = 30;

		var _width2 = _radius*2;
		var _height2 = _radius*2;

		var _point = {x:10,y:10,offsetX:10,offsetY:10};

		var _fullscreen_btn_clicked = false;
		
		var _touch_identifier = null;

		FullScreen = function(){

		};
		FullScreen.prototype = {
			constructor:FullScreen, 
			test:function(){
				console.log("hello from FullScreen");
			},
			start:function(){
				this.cargarSprites();
				this.configurarPosicionSprites();
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					$(w_ctouch).click(
						function(_evt){
							_this.listen(_evt);
						}
					);
				}
				window.addEventListener(
					"fullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"webkitfullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"mozfullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"fullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				window.addEventListener(
					"webkitfullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				window.addEventListener(
					"mozfullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				
			},
			onChange:function(evt){
				//console.log("change",evt);
			},
			onError:function(evt){
				//console.log("error",evt);
			},
			listen:function(evt){ 
				var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
				var _v2b = new THREE.Vector2(evt.clientX,evt.clientY);
				if(_v2a.distanceTo(_v2b)<=_radius){ 
					this.toggleFullScreen();  
					w_ctouch._lib_fullscreen_clicked = true;
				}else{
					w_ctouch._lib_fullscreen_clicked = false;
				}
			},
			toggleFullScreen:function() {
			  	if (!this.isWindowFullScreen()) {
			  		document.documentElement.requestFullscreen = 
			  			document.documentElement.webkitRequestFullscreen || 
			  			document.documentElement.mozRequestFullscreen || 
			  			document.documentElement.requestFullscreen;
			      	document.documentElement.requestFullscreen();
			  	}else{
			  		document.exitFullscreen = 
			  			document.exitFullscreen || 
			  			document.mozExitFullscreen || 
			  			document.webkitExitFullscreen;
			    	if (document.exitFullscreen) {
			      		document.exitFullscreen(); 
			    	}
			  	}
			},
			render:function(){
				this.dibujarSprites();
			},
			isWindowFullScreen:function(){
				return w_width == screen.width && w_height == screen.height;
			},
			resize:function(){
				this.configurarPosicionSprites();
			},
			configurarPosicionSprites:function(){
				_point.x = w_width - _width2 - _point.offsetX;
				_point.y = _point.offsetY;
			},
			cargarSprites:function(){
				if(!w_ctouch_ctx)return; 
				_btn_maximize = new Image;
				_btn_maximize.onload=function(){
					_btn_maximize._loaded = true;
				};	
				_btn_maximize.src = _btn_maximize_src;
				_btn_minimize = new Image; 
				_btn_minimize.onload=function(){
					_btn_minimize._loaded = true;
				};	
				_btn_minimize.src = _btn_minimize_src; 
			},
			dibujarSprites:function(){ 
				if(this.isWindowFullScreen()){
					if(w_ctouch_ctx && _btn_minimize && _btn_minimize._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_minimize,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 
				}else{
					if(w_ctouch_ctx && _btn_maximize && _btn_maximize._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_maximize,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 	
				} 
			},
			touch:function(_what,_evt){
				if(_what=="s")this.touch_start(_evt);
				if(_what=="e")this.touch_end(_evt);
				if(_what=="c")this.touch_cancel(_evt);
				if(_what=="l")this.touch_leave(_evt);
				if(_what=="m")this.touch_move(_evt);
			},
			touch_point_collide:function(_touch){
				if(_touch.clientX && _touch.clientY){
					var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
					var _v2b = new THREE.Vector2(_touch.clientX,_touch.clientY);
					if(_v2a.distanceTo(_v2b)<=_radius){ 
						return true;
					}
				}
				return false;
			},
			touch_start:function(_evt){ 
				if(_touch_identifier===null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(this.touch_point_collide(_touch)){
							_touch_play = true;
							_touch_identifier = _touch.identifier;
							break;
						}
					} 
				} 
			},
			touch_move:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							_touch_identifier==_touch.identifier 
						){
							if(this.touch_point_collide(_touch)){
								_touch_play = true; 
							}else{
								_touch_play = false;
							}
							break;
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier==_touch.identifier){
							_touch_play = false;
							_touch_identifier = null;
							break;
						}
					} 
				}  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new FullScreen();
					return _instance;
				}
				return null;
			}
		};
	}
)();
