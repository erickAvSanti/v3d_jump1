var appgame = appgame || {};

appgame.ZonaA = (
	function(){

		var _instance = null;
		var _loader = new THREE.JSONLoader();

		var _elements = {};  
		_elements["e2"] = "./modelos/e2.json";  

		var _pos_e1 = [
			new THREE.Vector3(22.830585479736328,1.1373908819223288*Math.pow(10,-6),14.97481918334961),
			new THREE.Vector3(-23.96137237548828,8.091782319752383*Math.pow(10,-7),-18.239112854003906),
			new THREE.Vector3(11.376300811767578,8.091782319752383*Math.pow(10,-7),-7.968557834625244),
			new THREE.Vector3(-11.427814483642578,8.091782319752383*Math.pow(10,-7),-3.4425506591796875),
			new THREE.Vector3(-53.38042449951172,8.091782319752383*Math.pow(10,-7),10.831779479980469),
			new THREE.Vector3(32.34911346435547,6.502324936263904*Math.pow(10,-7),33.993797302246094),
			new THREE.Vector3(9.78870964050293,6.110121262281609*Math.pow(10,-7),39.70909881591797),
			new THREE.Vector3(-26.00713539123535,6.708747832817608*Math.pow(10,-7),30.985740661621094),
			new THREE.Vector3(-28.51384735107422,-7.431228254972666*Math.pow(10,-8),-44.42450714111328),
			new THREE.Vector3(17.86032485961914,-1.032115104493414*Math.pow(10,-8),-37.94883346557617),
			new THREE.Vector3(-13.055790901184082,0.0,-36.90436935424805),
			new THREE.Vector3(4.003775596618652,0.0,-32.378360748291016),
			new THREE.Vector3(5.048238754272461,0.0,4.874161720275879),
		];

		var _objects = [];

		window._helice = null;
		window._parante_helice = null;

		var _helice_rot = Math.PI/54;

		ZonaA = function(){

		};
		ZonaA.prototype = {
			constructor:ZonaA,
			cargarElementos:function(){   


				for(var _idx in _elements){
					var _url = _elements[_idx];
					_instance.loadUrl(_url,_idx);
				}  
			}, 
			loadUrl:function(_url,_idx){ 
				_loader.load( 
					_url,  
					function ( geometry, materials ){   
						var obj = new THREE.Mesh( geometry, materials ); 
							obj._lib_name= _idx;
						if(_idx=="e1"){
							for(var _idy in _pos_e1){
								obj = obj.clone();
								obj.position.copy(_pos_e1[_idy]);
								obj.position.y=3; 
								w_wgl.scene.add( obj );   
								_objects.push(obj);
							}
						}else{
							w_wgl.scene.add( obj );  
							_objects.push(obj);
						}
						if(_idx=="e2"){
							window.w_e2 = obj;
						}

					},
					function(){

					},
					function(xhr){
						console.log(xhr);
					}
				);	
			},
			cancelLoad:function(){

			},
			render:function(){ 
				if(_helice && _parante_helice){
					_helice.position.copy(_parante_helice.position);
				}
				if(_helice){
					_helice.rotation.z+=_helice_rot;
				}
			},
			test:function(){
				console.log("hello from ZonaA object");
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new ZonaA();
					return _instance;
				}
				return null;
			}
		};
	}
)();
