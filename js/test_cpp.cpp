#include <iostream>

#include <ctime>
#include <cstdlib>
#include <string>
#include <vector>

using namespace std;

class Boat
{
  private:

        char dir;
        int length;
        vector<int> boatrow;
        vector<int> boatcol;
        int hits;
        string name;

    public:

        //Contstructor predeterminado
        Boat(char d, int lgth, vector<int> r, vector<int> c, int hit, string nme)
        {
            dir=d;
            length=lgth;
            boatrow=r;
            boatcol=c;
            hits=hit;
            name=nme;
        }

        //Establecer el golpe del bote
        void setHit()
        {
            hits++;
        }

        int checkSunk(int BoatSize)
        {
            if(hits>=BoatSize)
            {
                return 9;
            }
            else
            {
                return 0;
            }
        }

        //Obtener las coordenadas en la cuadrículo del barco
        void boatCoordinates()
        {
            cout << "Coordenadas para barco " << name << endl << endl;
            for(int i=0; i<length; i++)
            {
                cout << "Grid [" << boatrow[i] << "][" << boatcol[i] << "]" << endl;
            }
        cout << endl;
        }

        //Verifica las coordenadas bombardeadas para encontrar un barco en particular
        string getBoat(int r, int c)
        {
            for(int i=0; i<length; i++)
            {
                if((boatrow[i]==r) && (boatcol[i]==c))
                {
                    return name;
                }
            }
            return "";
         }
};

enum BoatSize { Submarine = 2, Destroyer = 3, Battleship = 4, Carrier = 5 };
void initGrid(int grid[][10]);
void printBoard(int grid[][10]);
void printGameBoard(int grid[][10]);
int resetColAndRow(int col, int &row, int BoatSize, char d);
char getDirection(int d);
int checkSpaces(int grid[][10], int c, int r, int s, char d);
void editGrid(int grid[][10], int col, int row, int BoatSize, char dir);
bool setBoat(int grid[][10], int BoatSize, int name, vector<Boat> &boatList);
void editBoatInfo(int grid[][10], int c, int r, int BoatSize, char d, vector<Boat> &boatList, int name);
int playGame(int grid[][10], vector<Boat> &boatList);
int getSpace(int grid[][10], int row, int col);

int main()
{
    int grid[10][10];
    vector<Boat> boatList;
    char play;
    initGrid(grid);
//    printBoard(grid); Descomentar para ver la matriz inicializada
    setBoat(grid, Carrier, 1, boatList);  //Colocar los barcos en la matriz
    setBoat(grid, Battleship, 2, boatList);
    setBoat(grid, Battleship, 3, boatList);
    setBoat(grid, Destroyer, 4, boatList);
    setBoat(grid, Destroyer, 5, boatList);
    setBoat(grid, Destroyer, 6, boatList);
    setBoat(grid, Submarine, 7, boatList);
    setBoat(grid, Submarine, 8, boatList);
    setBoat(grid, Submarine, 9, boatList);
    setBoat(grid, Submarine, 10, boatList);

    cout << "Bienvenido a Battleship.  Presiona la tecla c para empezar a jugar!" << endl << endl;
    cout << "¿Lograrás derribar la flota enemiga?" << endl << endl;
    cout << "La pantalla te mostrará 1 si aciertas tu tiro y 9 si fallas" << endl << endl;
    cin >> play;
    if(play=='c')
    {
        playGame(grid, boatList);
    }
    return 0;
}

void initGrid(int grid[][10])
{
    for(int col=0; col<10; col++) //Bucle de columna exterior
    {
        for(int row=0; row<10; row++) //Bucle de fila interna
        {
            grid[col][row]=0;
        }
    }
}

void printBoard(int grid[][10])  //Imprime el tablero con los botes puestos
{
    cout << "   0|1|2|3|4|5|6|7|8|9" << endl << endl;
    for(int i=0; i<10; i++)  //Bucle de la columna
    {
        for(int j=0; j<10; j++)  //Bucle de la fila
        {
            if(j==0)
            {
                cout << i << "  " ; //Imprime el número de fila y espacios antes de la nueva fila
            }
            cout << grid[i][j] ;
            if(j!=9)
            {
                cout << "|";
            }
        }
    cout << endl; //Nueva línea al final de la columna
    }
    cout << endl;
}

void printGameBoard(int grid[][10]) //Este es el tablero que se imprime para jugar el juego. No puedes ver los barcos
{
    cout << "   0|1|2|3|4|5|6|7|8|9" << endl << endl;
    for(int i=0; i<10; i++)  //Bucle de la columna
    {
        for(int j=0; j<10; j++)  //Bucle de la fila
        {
            if(j==0)
            {
                cout << i << "  " ; //Imprime el número de fila y los espacios antes de la nueva fila
            }
            if(grid[i][j]==1)  //si el espacio ha sido impactado y está el barco, impríme 1
            {
                cout << 1;
            } else if(grid[i][j]==9)  //si el espacio ha sido impactado y es un tiro fallido imprime 9
            {
                cout << 9;
            } else
            {
                cout << 0;  //De lo contrario, simplemente imprime un 0
            }

            if(j!=9)
            {
                cout << "|";
            }
        }
    cout << endl; //Nueva línea al final de la columna
    }
    cout << endl;
}

bool setBoat(int grid[][10], int BoatSize, int name, vector<Boat> &boatList)
//Esta función coloca los barcos individuales en la grilla inicializada
{
    srand(time(0));
    int col=0;
    int row=0;
    char d='v';
    bool placementFailure=true;
    char check='v';
    int cS=0;

    d=getDirection(rand()%10);  //Elige al azar en qué dirección colocar el bote
    col=resetColAndRow(col, row, BoatSize, d);  //Esta función devuelve una columna aleatoria y una fila (por referencia) de donde se va a colocar el bote
                                               
    while(placementFailure)
    {
        if(d=='h')
        {
            cS=checkSpaces(grid, col, row, BoatSize, d);//Verifica que el barco se pueda colocar sin superponer otro barco
            if(cS==1)//Si el barco se superpone, genera otra columna aleatoria, fila y dirección y comienza nuevamente el ciclo
            {
                d=getDirection(rand()%10);
                col=resetColAndRow(col, row, BoatSize, d);
                cS==0;
                continue;
            }
            editGrid(grid, col, row, BoatSize, d);//Coloca el bote en la matriz
            editBoatInfo(grid, col, row, BoatSize, d, boatList, name);//Crea el objeto del bote
            return 0;
        }// fin de 'si es horizontal'
        else if(d=='v')
        {
            cS=checkSpaces(grid, col, row, BoatSize, d);
            if(cS==1)
            {
                d=getDirection(rand()%10);
                col=resetColAndRow(col, row, BoatSize, d);
                cS==0;
                continue;
            }
            editGrid(grid, col, row, BoatSize, d);
            editBoatInfo(grid, col, row, BoatSize, d, boatList, name);
            return 0;
        }
     }//final del ciclo while
}//fin de la función setBoat

char getDirection(int d)
{
    if(d>4)
        {
            return 'h';  //elige al azar en qué dirección colocar el bote
        }
        else
        {
            return 'v';
        }
}

void editGrid(int grid[][10], int col, int row, int BoatSize, char dir)
//Esta función pone los números que corresponden al tipo de barco en la matriz
{
    if(dir=='h')
    {
        for(int i=0; i<BoatSize; i++)
        {
            grid[row][col]=BoatSize;
            col++;
            cout << endl;
        }
    }
    else if(dir=='v')
    {
        for(int i=0; i<BoatSize; i++)
        {
            grid[row][col]=BoatSize;
            row++;
            cout << endl;
        }
    }
    else
    {
        cout << "Error! Dirección no pasada" << endl;
    }
    //printBoard(grid);  //descomentar para ver la matriz terminada
}

int checkSpaces(int grid[][10], int c, int r, int s, char d)
//Comprueba la matriz para asegurarse de que ninguno de los barcos se superpondrá
{
    int check=0;
    if(d=='h')
    {
        for(int i=c; i<c+s; i++)
        {
        check=grid[r][i];
                if(check!=0)
                {
                    return 1;
                }
        }

        return 0;
    }
    else
    {
        for(int i=r; i<r+s; i++)
        {
        check=grid[i][c];
                if(check!=0)
                {
                    return 1;

                }
        }

        return 0;
    }
}

int resetColAndRow(int col, int &row, int BoatSize, char d)
{
    switch(BoatSize) //Genera columnas y filas aleatorias según el tamaño de la embarcación para que no sobrepasemos el borde de la matriz
    {
        case Submarine:
            if(d=='h')
            {
                col=rand()%8;
                row=rand()%10;
            }
            else
            {
                col=rand()%10;
                row=rand()%8;
            }
            break;
        case Destroyer:
            if(d=='h')
            {
                col=rand()%7;
                row=rand()%10;
            }
            else
            {
                col=rand()%10;
                row=rand()%7;
            }
            break;
        case Battleship:
            if(d=='h')
            {
                col=rand()%6;
                row=rand()%10;
            }
            else
            {
                col=rand()%10;
                row=rand()%6;
            }
            break;
        case Carrier:
            if(d=='h')
            {
                col=rand()%5;
                row=rand()%10;
            }
            else
            {
                col=rand()%10;
                row=rand()%5;
            }
    }
    return col;
}

void editBoatInfo(int grid[][10], int c, int r, int BoatSize, char d, vector<Boat> &boatList, int name)
//Esta función crea los objetos del barco
{
    switch(name)
    {
        case 1:
            if(d=='h')
            {
                vector<int> r1 (5);
                //  Pone los datos de coordenadas en los vectores usando .at()
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (5);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
            //Portaaviones
            Boat carrierBoat('h', 5, r1, c1, 0, "Portaaviones");
            boatList.push_back(carrierBoat);
            }
            else if(d=='v')
            {
                vector<int> r1 (5);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (5);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
            //Portaaviones
            Boat carrierBoat('v', 5, r1, c1, 0, "Portaaviones");
            boatList.push_back(carrierBoat);
            }
            break;
        case 2:
            if(d=='h')
            {
                vector<int> r1 (4);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (4);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
            //Buque de guerra 1
            Boat battleship1Boat('h', 4, r1, c1, 0, "Buque de guerra 1");
            boatList.push_back(battleship1Boat);
            }
            else if(d=='v')
            {
                vector<int> r1 (4);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (4);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
            //Buque de guerra 1
            Boat battleship1Boat('v', 4, r1, c1, 0, "Buque de guerra 1");
            boatList.push_back(battleship1Boat);
        }
        break;
        case 3:
            if(d=='h')
            {
                vector<int> r1 (4);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (4);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
            //Buque de guerra 2
            Boat battleship2Boat('h', 4, r1, c1, 0, "Buque de guerra 2");
            boatList.push_back(battleship2Boat);
            }
            else if(d=='v')
            {
                vector<int> r1 (4);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (4);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
            //Buque de guerra 2
            Boat battleship2Boat('v', 4, r1, c1, 0, "Buque de guerra 2");
            boatList.push_back(battleship2Boat);
            }
            break;
        case 4:
            if(d=='h')
            {
                vector<int> r1 (3);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (3);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
             //Destructor 1
             Boat destroyer1Boat('h', 3, r1, c1, 0, "Destructor 1");
             boatList.push_back(destroyer1Boat);
             }
             else if(d=='v')
             {
                vector<int> r1 (3);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (3);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
             //Destructor 1
             Boat destroyer1Boat('v', 3, r1, c1, 0, "Destructor 1");
             boatList.push_back(destroyer1Boat);
             }
             break;
        case 5:
        if(d=='h')
        {
            vector<int> r1 (3);
            for (int i=0; i<(int)r1.size(); ++i)
            {
                r1.at(i)=r;
            }
            vector<int> c1 (3);
            for (int i=0; i<(int)c1.size(); ++i)
            {
                c1.at(i)=c;
                c++;
            }
        //Destructor 2
        Boat destroyer2Boat('h', 3, r1, c1, 0, "Destructor 2");
        boatList.push_back(destroyer2Boat);
        }
        else if(d=='v')
        {
            vector<int> r1 (3);
            for (int i=0; i<(int)r1.size(); ++i)
            {
                r1.at(i)=r;
                r++;
            }
            vector<int> c1 (3);
            for (int i=0; i<(int)c1.size(); ++i)
            {
                c1.at(i)=c;
            }
            //Destructor 2
            Boat destroyer2Boat('v', 3, r1, c1, 0, "Destructor 2");
            boatList.push_back(destroyer2Boat);
        }
            break;
        case 6:
            if(d=='h')
            {
                vector<int> r1 (3);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (3);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
            //Destructor 3
            Boat destroyer3Boat('h', 3, r1, c1, 0, "Destructor 3");
            boatList.push_back(destroyer3Boat);
            }
            else if(d=='v')
            {
                vector<int> r1 (3);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (3);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
            //Destructor 3
            Boat destroyer3Boat('v', 3, r1, c1, 0, "Destructor 3");
            boatList.push_back(destroyer3Boat);
            }
            break;
        case 7:
            if(d=='h')
            {
                vector<int> r1 (2);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (2);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
            //Submarino 1
            Boat submarine1Boat('h', 2, r1, c1, 0, "Submarino 1");
            boatList.push_back(submarine1Boat);
            }
            else if(d=='v')
            {
                vector<int> r1 (2);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (2);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
            //Submarino 1
            Boat submarine1Boat('v', 2, r1, c1, 0, "Submarino 1");
            boatList.push_back(submarine1Boat);
            }
            break;
        case 8:
            if(d=='h')
            {
                vector<int> r1 (2);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (2);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
            //Submarino 2
            Boat submarine2Boat('h', 2, r1, c1, 0, "Submarino 2");
            boatList.push_back(submarine2Boat);
            }
            else if(d=='v')
            {
                vector<int> r1 (2);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (2);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
            //Submarino 2
            Boat submarine2Boat('v', 2, r1, c1, 0, "Submarino 2");
            boatList.push_back(submarine2Boat);
            }
            break;
        case 9:
            if(d=='h')
            {
                vector<int> r1 (2);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                }
                vector<int> c1 (2);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                    c++;
                }
            //Submarino 3
            Boat submarine3Boat('h', 2, r1, c1, 0, "Submarino 3");
            boatList.push_back(submarine3Boat);
            }
            else if(d=='v')
            {
                vector<int> r1 (2);
                for (int i=0; i<(int)r1.size(); ++i)
                {
                    r1.at(i)=r;
                    r++;
                }
                vector<int> c1 (2);
                for (int i=0; i<(int)c1.size(); ++i)
                {
                    c1.at(i)=c;
                }
            //Submarino 3
            Boat submarine3Boat('v', 2, r1, c1, 0, "Submarino 3");
            boatList.push_back(submarine3Boat);
            }
            break;
        case 10:
        if(d=='h')
        {
            vector<int> r1 (2);
            for (int i=0; i<(int)r1.size(); ++i)
            {
                r1.at(i)=r;
            }
            vector<int> c1 (2);
            for (int i=0; i<(int)c1.size(); ++i)
            {
                c1.at(i)=c;
                c++;
            }
        //Submarino 4
        Boat submarine4Boat('h', 2, r1, c1, 0, "Submarino 4");
        boatList.push_back(submarine4Boat);
        }
        else if(d=='v')
        {
            vector<int> r1 (2);
            for (int i=0; i<(int)r1.size(); ++i)
            {
                r1.at(i)=r;
                r++;
            }
            vector<int> c1 (2);
            for (int i=0; i<(int)c1.size(); ++i)
            {
                c1.at(i)=c;
            }
        //Submarino 5
        Boat submarine4Boat('v', 2, r1, c1, 0, "Submarino 5");
        boatList.push_back(submarine4Boat);
        }
        break;
    }
}

int playGame(int grid[][10], vector<Boat> &boatList)
{
    bool gameInProgress=true;
    int row=0;
    int col=0;
    int guess=0;
    int hit=0;
    int miss=0;
    int space=0;
    char d='g';
    string btname="";
    int sunk=0;

    while(gameInProgress)
    {
        printGameBoard(grid);
        //printBoard(grid);  //descomentar ver el tablero de juego con los barcos en él
        cout << "Ingrese una coordenada de fila: ";
        cin >> row;
        cout << "Ingrese una coordenada de columna: ";
        cin >> col;
        cout << endl;
        guess++;
        space=getSpace(grid, row, col);

        switch(space)
        {
            case 0:
                cout << "Haz fallado!" << endl;
                grid[row][col]=9;
                miss++;
                break;
            case 1:
                cout << "Este espacio ya ha sido bombardeado. ¡Has desperdiciado un disparo!" << endl;
                break;
            case 2:
                grid[row][col]=1;
                hit++;

                btname=boatList[6].getBoat(row, col);  //Compruebe si el barco es submarino 1
                if(btname=="Submarine 1")
                {
                    cout << "Has acertado " << btname << "!" << endl;
                    boatList[6].setHit();
                    sunk=boatList[6].checkSunk(Submarine);
                    if(sunk==9)
                    {
                        cout << "Has hundido el submarino 1!" << endl;
                    }
                } else if(btname.empty())
                {
                    btname=boatList[7].getBoat(row, col);  //Compruebe si el barco es submarino 2
                    if(btname=="Submarine 2")
                    {
                        cout << "Has acertado " << btname << "!" << endl;
                        boatList[7].setHit();
                        sunk=boatList[7].checkSunk(Submarine);
                        if(sunk==9)
                        {
                            cout << "Has hundido el submarino 2!" << endl;
                        }
                    } else if(btname.empty())
                    {
                        btname=boatList[8].getBoat(row, col);  //Compruebe si el barco es submarino 3
                        cout << "Has acertado" << btname << "!" << endl;
                        boatList[8].setHit();
                        sunk=boatList[8].checkSunk(Submarine);
                        if(sunk==9)
                        {
                            cout << "Has hundido el submarino 3!" << endl;
                        }
                    } else if(btname.empty())
                    {
                        btname=boatList[9].getBoat(row, col);  //Compruebe si el barco es submarino 4
                        cout << "Has acertado " << btname << "!" << endl;
                        boatList[9].setHit();
                        sunk=boatList[9].checkSunk(Submarine);
                        if(sunk==9)
                        {
                            cout << "Has hundido el submarino 4!" << endl;
                        }
                    }
                }
                btname.clear();
                break;
            case 3:
                grid[row][col]=1;
                hit++;

                btname=boatList[3].getBoat(row, col);  //Verifica si el barco es Destructor 1
                if(btname=="Destroyer 1")
                {
                    cout << "Has acertado " << btname << "!" << endl;
                    boatList[3].setHit();
                    sunk=boatList[3].checkSunk(Destroyer);
                    if(sunk==9)
                    {
                        cout << "Has hundido el destructor 1!" << endl;
                    }
                } else if(btname.empty())
                {
                    btname=boatList[4].getBoat(row, col);  //Verifica si el barco es Destructor 2
                    if(btname=="Destroyer 2")
                    {
                        cout << "Has acertado " << btname << "!" << endl;
                        boatList[4].setHit();
                        sunk=boatList[4].checkSunk(Destroyer);
                        if(sunk==9)
                        {
                            cout << "Has hundido el destructor 2!" << endl;
                        }
                    } else if(btname.empty())
                    {
                        btname=boatList[5].getBoat(row, col);  //Comprueba si el barco es el Destructor 3
                        cout << "Has acertado" << btname << "!" << endl;
                        boatList[5].setHit();
                        sunk=boatList[5].checkSunk(Destroyer);
                        cout << "hundido es " << sunk << endl;
                        if(sunk==9)
                        {
                            cout << "Has hundido el destructor 3" << endl;
                        }
                    }
                }
                btname.clear();
                break;
            case 4:
                grid[row][col]=1;
                hit++;

                btname=boatList[1].getBoat(row, col);  //Compruebe si el barco es buque de guerra 1
                if(btname=="Battleship 1")
                {
                    cout << "Has acertado " << btname << "!" << endl;
                    boatList[1].setHit();
                    sunk=boatList[1].checkSunk(Battleship);
                    if(sunk==9)
                    {
                        cout << "Has hundido el buque de guerra 1" << endl;
                    }
                }
                if(btname.empty())
                {
                    btname=boatList[2].getBoat(row, col);  //Compruebe si el barco es buque de guerra 2
                    cout << "Has acertado " << btname << "!" << endl;
                    boatList[2].setHit();
                    sunk=boatList[2].checkSunk(Battleship);
                    if(sunk==9)
                    {
                        cout << "Has hundido el buque de guerra 2" << endl;
                    }
                }
                btname.clear();
                break;
            case 5:
                cout << "¡Has acertado al portaaviones!" << endl;
                grid[row][col]=1;
                hit++;
                boatList[0].setHit();
                sunk=boatList[0].checkSunk(Carrier);
                if(sunk==9)
                {
                    cout << "Has derribado el portaaviones" << endl;
                }
                break;
        }//Final del switch
        if(hit==30)
        {
            gameInProgress=false;
        }
    }//Final del juego mientras el bucle está jugando

    cout << "¡Gracias por jugar! Has hundido todos mis barcos en " << guess << " movimientos!" << endl;

}//final de la función playGame 

int getSpace(int grid[][10], int row, int col)
{
    int space=0;
    space=grid[row][col];
    return space;
}