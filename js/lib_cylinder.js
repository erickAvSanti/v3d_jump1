/*
	
	var _ccylinder = new CCylinder(
			new THREE.Vector3(0,1,0),
			new THREE.Vector3(0,0,1),
			new THREE.Vector3(-10,40,0),
			10,
			1, 
			);

*/

CCylinder = function(
		vunit_up,
		vunit_front,
		origin,
		height, 
		radius,
	){
	this.height = height || 10; //integer
	this.radius = radius || 2; //integer
	this.velocity = 0;
	this.gravity = 10;

	this.pointsGeometry = new THREE.Geometry();
	this.pointsMaterial = new THREE.PointsMaterial( { color: 0x888888,size:0.5 } );
	this.points = new THREE.Points( this.pointsGeometry, this.pointsMaterial );

	this._vX = new THREE.Vector3(1,0,0);
	this._vY = new THREE.Vector3(0,1,0);
	this._vZ = new THREE.Vector3(0,0,1);
 
	
	this.vunit_up = vunit_up.clone().normalize();
	this.vunit_front = vunit_front.clone().normalize();
	this.origin = origin.clone();
	this.originTop = origin.clone().addScaledVector(this.vunit_up,this.height);
	this.last_origin = origin.clone();
  
	this._points_per_circle = 24;
	this.calc();

	this._lineProjectedOntoBaseCylinder = new THREE.Line3();
	this._baseSphere = new THREE.Sphere(this.origin,this.radius);

	this._tmpVector3 = new THREE.Vector3();

	this._tmpVector3_1 = new THREE.Vector3();
	this._tmpVector3_2 = new THREE.Vector3();

	this._tmpLineTop = new THREE.Line3(new THREE.Vector3(),new THREE.Vector3());
	this._tmpLineTop.vunit = new THREE.Vector3(0,1,0);

	this._tmpLine = new THREE.Line3(new THREE.Vector3(),new THREE.Vector3());
	this._tmpLine.vunit = new THREE.Vector3(0,1,0);

	this._zeroProx = 0.001;
};
CCylinder.prototype = {
	setPlane:function(){
		if(!this.plane){
			this.plane = new THREE.Plane(new THREE.Vector3(0,1,0),1);
			this.planeNeedsUpdate = true;
		}
		if(this.planeNeedsUpdate){
			this.plane.normal.copy(this.vunit_up); 
			this.plane.constant = this.origin.dot(this.vunit_up);
			this.planeNeedsUpdate = false;
		}
	},
	projectLineOntoPlane:function(line3,targetLine3){
		if(!targetLine3){
			targetLine3 = this._lineProjectedOntoBaseCylinder;
		} 
		this.plane.projectPoint(line3.start,targetLine3.start);
		this.plane.projectPoint(line3.end,targetLine3.end); 

		if(!targetLine3.vunit)targetLine3.vunit = new THREE.Vector3();

		targetLine3.vunit.copy(targetLine3.end).sub(targetLine3.start).normalize();
		return targetLine3;
	},
	sphereLineIntersectionPoints:function(line3,onlyCheck){
		var _sphereCenter = this._baseSphere.center;
		var _sphereRadius = this._baseSphere.radius;

		if(!line3)line3 = this._lineProjectedOntoBaseCylinder;

		var _oc = line3.start.clone().sub(_sphereCenter);

		var _loc = _oc.dot(line3.vunit);

		var _a = line3.vunit.lengthSq();
		var _b = _loc*2;
		var _c = _oc.lengthSq() - Math.pow(_sphereRadius,2);

		var _delta = Math.pow(_loc,2) + Math.pow(_sphereRadius,2) - _oc.lengthSq();
	
		var _d =  - _loc;

		if(_delta<0){
			if(onlyCheck){
				return false;
			}
			return null;
		}else if(_delta<this._zeroProx){//one solution
			if(onlyCheck){
				return true;
			}
			return this._tmpVector3_1.copy(line3.start).addScaledVector(line3.vunit,_d);
		}else{
			if(onlyCheck){
				return true;
			}
			_delta = Math.sqrt(_delta);
			var _d1 = _d + _delta;
			var _d2 = _d - _delta;
			this._tmpVector3_2.copy(line3.start).addScaledVector(line3.vunit,_d1);
			this._tmpVector3_1.copy(line3.start).addScaledVector(line3.vunit,_d2);
		}
		var _tmp_line = new THREE.Line3(this._tmpVector3_1,this._tmpVector3_2);
		_tmp_line.vunit = line3.vunit.clone();
		return _tmp_line;
	},
	calcCropPoints:function(line3){ 
		this._tmpLineTop.start.copy(line3.start).addScaledVector(this.vunit_up,this.height);
		this._tmpLineTop.end.copy(line3.end).addScaledVector(this.vunit_up,this.height);
		this._tmpLineTop.vunit.copy(line3.vunit); 
		return this._tmpLineTop;
	},
	pointInsideQuadrilateral:function(point,origin,base,radius){
		var _vector = point.clone().sub(origin);
		var _projectUp = _vector.clone().projectOnVector(this.vunit_up);
		var _projectBase = _vector.clone().projectOnVector(base);

		return this.vunit_up.dot(_vector)>=0 && 
		_projectBase.length()<=radius && 
		_projectUp.length()<=this.height;
	},
	intersectsPlane:function(plane){//THREE.Plane from CBox
		var _intersection = this.linePlaneIntersection(plane,this.vunit_up,this.origin); 
		plane.projectPoint(this.origin,this._tmpLine.start);
		plane.projectPoint(this.originTop,this._tmpLine.end);
		if(_intersection===1 || _intersection===-1){ 

			return 
				plane.tri1.containsPoint(this._tmpLine.start) || 
				plane.tri1.containsPoint(this._tmpLine.end) || 
				plane.tri2.containsPoint(this._tmpLine.start) || 
				plane.tri2.containsPoint(this._tmpLine.end);
		}/*else if(_intersection==-1){

		}*/else{
			return this.intersectsLine(this._tmpLine);
		}
	},
	linePlaneIntersection:function(plane,line_vunit,line_l0,return_point = false){
		var p0l0 = this._tmpVector3.copy(plane.p0).sub(line_l0);
		var p0l0dotn = p0l0.dot(line_vunit);
		var ln = line_vunit.dot(plane.normal);
		var _intersection;
		if(Math.abs(ln)<this._zeroProx){//parallel
			if(Math.abs(p0l0dotn)<this._zeroProx){//line contained in plane
				_intersection = 1;
			}else{
				//no intersection
				_intersection = -1;
			}
		}else{
			//one intersection
			if(return_point){
				var d = p0l0dotn/ln;
				_intersection = line_vunit.clone().multiplyScalar(d).add(line_l0);
			}else{
				_intersection = 0;
			}
		}
		return _intersection;
	},
	intersectsLine:function(line3){
		this.setPlane();
		this.projectLineOntoPlane(line3);
		var _line3Returned = this.sphereLineIntersectionPoints();
		if(_line3Returned && _line3Returned instanceof THREE.Line3){
			var _topLine = this.calcCropPoints(_line3Returned);
			var _mn = _line3Returned.end.clone().sub(_line3Returned.start);
			var _tmpRadius = _mn.length()/2;
			var _centerLine3 = _mn.clone().multiplyScalar(0.5).add(_line3Returned.start);

			var _p1 = new THREE.Vector3();
			var _p2 = new THREE.Vector3();
			var _p3 = new THREE.Vector3();
			var _p4 = new THREE.Vector3();

			line3.closestPointToPoint(_line3Returned.start,true,_p1);
			line3.closestPointToPoint(_line3Returned.end,true,_p2);
			line3.closestPointToPoint(_topLine.start,true,_p3);
			line3.closestPointToPoint(_topLine.end,true,_p4);

			return this.pointInsideQuadrilateral(_p1,_centerLine3,_mn,_tmpRadius) || 
			this.pointInsideQuadrilateral(_p2,_centerLine3,_mn,_tmpRadius) || 
			this.pointInsideQuadrilateral(_p3,_centerLine3,_mn,_tmpRadius) || 
			this.pointInsideQuadrilateral(_p4,_centerLine3,_mn,_tmpRadius);

		}
		return false;
	},
	intersectsPoint:function(point){
		this._tmpVector3.copy(point);
		var _vector = this._tmpVector3.sub(this.origin);
		this._tmpVector3_1.copy(_vector);
		this._tmpVector3_2.copy(_vector);
		var _vectorUp = this._tmpVector3_1.projectOnVector(this.vunit_up);
		var _vectorBase = this._tmpVector3_2.sub(_vectorUp);

		return this.vunit_up.dot(_vector)>=0 &&_vectorUp.length()<=this.height && _vectorBase.length()<=this.radius;
	},
	intersectCBox:function(box){
		if(this.intersectsPoint(box.pointA)){
			box.objIntersection = box.pointA;
		}else if(this.intersectsPoint(box.pointB)){
			box.objIntersection = box.pointB;
		}else if(this.intersectsPoint(box.pointC)){
			box.objIntersection = box.pointC;
		}else if(this.intersectsPoint(box.pointD)){
			box.objIntersection = box.pointD;
		}

		else if(this.intersectsPoint(box.pointE)){
			box.objIntersection = box.pointE;
		}else if(this.intersectsPoint(box.pointF)){
			box.objIntersection = box.pointF;
		}else if(this.intersectsPoint(box.pointG)){
			box.objIntersection = box.pointG;
		}else if(this.intersectsPoint(box.pointH)){
			box.objIntersection = box.pointH;
		}

		else if(this.intersectsLine(box._tmpLineAB)){
			box.objIntersection = box._tmpLineAB;
		}else if(this.intersectsLine(box._tmpLineBC)){
			box.objIntersection = box._tmpLineBC;
		}else if(this.intersectsLine(box._tmpLineCD)){
			box.objIntersection = box._tmpLineCD;
		}else if(this.intersectsLine(box._tmpLineDA)){
			box.objIntersection = box._tmpLineDA;
		}

		else if(this.intersectsLine(box._tmpLineEF)){
			box.objIntersection = box._tmpLineEF;
		}else if(this.intersectsLine(box._tmpLineFG)){
			box.objIntersection = box._tmpLineFG;
		}else if(this.intersectsLine(box._tmpLineGH)){
			box.objIntersection = box._tmpLineGH;
		}else if(this.intersectsLine(box._tmpLineHE)){
			box.objIntersection = box._tmpLineHE;
		}

		else if(this.intersectsLine(box._tmpLineAE)){
			box.objIntersection = box._tmpLineAE;
		}else if(this.intersectsLine(box._tmpLineBF)){
			box.objIntersection = box._tmpLineBF;
		}else if(this.intersectsLine(box._tmpLineCG)){
			box.objIntersection = box._tmpLineCG;
		}else if(this.intersectsLine(box._tmpLineDH)){
			box.objIntersection = box._tmpLineDH;
		}

		else if(this.intersectsPlane(box._tmpPlaneUp)){
			box.objIntersection = box._tmpPlaneUp;
		}else if(this.intersectsPlane(box._tmpPlaneBottom)){
			box.objIntersection = box._tmpPlaneBottom;
		}else if(this.intersectsPlane(box._tmpPlaneLeft)){
			box.objIntersection = box._tmpPlaneLeft;
		}else if(this.intersectsPlane(box._tmpPlaneRight)){
			box.objIntersection = box._tmpPlaneRight;
		}else if(this.intersectsPlane(box._tmpPlaneFront)){
			box.objIntersection = box._tmpPlaneFront;
		}else if(this.intersectsPlane(box._tmpPlaneBack)){
			box.objIntersection = box._tmpPlaneBack;
		}

		else if(this.insideCBox(box)){
			box.objIntersection = box;
		}else{
	 		box.objIntersection = null;
		} 
	},
	insideCBox:function(box){
		
		return 

		box._tmpPlaneUp.normal.dot(this.origin)<=0 && 
		box._tmpPlaneBottom.normal.dot(this.origin)<=0 && 
		box._tmpPlaneLeft.normal.dot(this.origin)<=0 && 
		box._tmpPlaneRight.normal.dot(this.origin)<=0 && 
		box._tmpPlaneFront.normal.dot(this.origin)<=0 && 
		box._tmpPlaneBack.normal.dot(this.origin)<=0 && 

		box._tmpPlaneUp.normal.dot(this.originTop)<=0 && 
		box._tmpPlaneBottom.normal.dot(this.originTop)<=0 && 
		box._tmpPlaneLeft.normal.dot(this.originTop)<=0 && 
		box._tmpPlaneRight.normal.dot(this.originTop)<=0 && 
		box._tmpPlaneFront.normal.dot(this.originTop)<=0 && 
		box._tmpPlaneBack.normal.dot(this.originTop)<=0;
	},
	calc:function(){
		this.vunit_left = this.vunit_front.clone().cross(this.vunit_up).normalize();
		this.vunit_front.copy(this.vunit_left).applyAxisAngle(this.vunit_up,Math.PI/2).normalize();
		this._setPoints();
	},
	moveTo:function(x,y,z){
		this.last_origin.copy(this.origin);
		this.origin.set(x,y,z);
		this.planeNeedsUpdate = true;
		this._setPoints();
	},
	_setPoints:function(){ 
		//this.pointsGeometry.vertices.splice(0,this.pointsGeometry.vertices.length);
		var _new = this.pointsGeometry.vertices.length==0; 
		this.originTop.copy(this.origin).addScaledVector(this.vunit_up,this.height);
		var _vectorRad = this.vunit_left.clone().multiplyScalar(this.radius);
		
		var _circles = [];

		for(var j=0;j<this._points_per_circle;j++){
			var _v1 = _vectorRad.clone()
			.applyAxisAngle(this.vunit_up,j*2*Math.PI/this._points_per_circle);
			_circles.push(_v1);  
		}

		var _index = 0;

		for(var i=0; i<this.height;i++){
			for(var idx in _circles){
				var _vertex = _circles[idx].clone()
				.addScaledVector(this.vunit_up,i)
				.add(this.origin); 
				if(_new){
					this.pointsGeometry.vertices.push(_vertex);
				}else{
					this.pointsGeometry.vertices[_index].copy(_vertex);
				}
				_index++;
			} 
		}
	 	this.pointsGeometry.verticesNeedUpdate = true;

	},
	rotX: function(angle,needsupdate){
		this.vunit_up.applyAxisAngle(this._vX,angle).normalize();
		this.vunit_front.applyAxisAngle(this._vX,angle).normalize();
		this.vunit_left.applyAxisAngle(this._vX,angle).normalize();
		this.planeNeedsUpdate = true;
		if(needsupdate)this._setPoints();

	},
	rotY: function(angle,needsupdate){
		this.vunit_up.applyAxisAngle(this._vY,angle).normalize();
		this.vunit_front.applyAxisAngle(this._vY,angle).normalize();
		this.vunit_left.applyAxisAngle(this._vY,angle).normalize();
		this.planeNeedsUpdate = true;
		if(needsupdate)this._setPoints();
	},
	rotZ: function(angle,needsupdate){
		this.vunit_up.applyAxisAngle(this._vZ,angle).normalize();
		this.vunit_front.applyAxisAngle(this._vZ,angle).normalize();
		this.vunit_left.applyAxisAngle(this._vZ,angle).normalize();
		this.planeNeedsUpdate = true;
		if(needsupdate)this._setPoints();
	},
	rot:function(angleX,angleY,angleZ){
		var _euler = new THREE.Euler( angleX,angleY,angleZ, 'XYZ' );
		this.vunit_up.applyEuler(_euler).normalize();
		this.vunit_front.applyEuler(_euler).normalize();
		this.vunit_left.applyEuler(_euler).normalize(); 
		this.planeNeedsUpdate = true;
		this._setPoints();
	},
	attachToScene:function(scene){
		if(this.attached)return;
		this.attached = true; 
		scene.add(this.points);  
	},
};