var appgame = appgame || {};

appgame.Bloques = (
	function(){

		var _instance = null;  

		var _loader = new THREE.JSONLoader(); 

		var _models = {
			"m1":"./modelos/bloque.json",
			"m2":"./modelos/bloque2.json",
			"m3":"./modelos/bloque3.json"
		};

		window._lib_bm2 = [];
		window._lib_bm3 = [];

		window._lib_spheres_blocks = [/*for collisions*/
			new THREE.Sphere(new THREE.Vector3(13.606074333190918,4.831060409545898,10.539889335632324),12.8605),
			new THREE.Sphere(new THREE.Vector3(12.888176918029785,19.70177459716797,10.539889335632324),12.8605),
			new THREE.Sphere(new THREE.Vector3(-7.739987373352051,19.7017822265625,11.390525817871094),12.8605),
			new THREE.Sphere(new THREE.Vector3(-7.022089958190918,4.831068992614746,11.390525817871094),12.8605),
			new THREE.Sphere(new THREE.Vector3(-6.384105682373047,4.831068515777588,-24.549270629882812),12.8605),
			new THREE.Sphere(new THREE.Vector3(-7.10200309753418,19.7017822265625,-24.549270629882812),12.8605),
			new THREE.Sphere(new THREE.Vector3(12.675514221191406,19.70177459716797,-22.422645568847656),12.8605),
			new THREE.Sphere(new THREE.Vector3(13.393411636352539,4.831060409545898,-22.422645568847656),12.8605),
			new THREE.Sphere(new THREE.Vector3(21.049224853515625,4.831057071685791,-6.898349761962891),12.8605),
			new THREE.Sphere(new THREE.Vector3(20.331327438354492,19.701770782470703,-6.898349761962891),12.8605),
			new THREE.Sphere(new THREE.Vector3(-13.907179832458496,19.701770782470703,-6.898334503173828),12.8605),
			new THREE.Sphere(new THREE.Vector3(-13.189282417297363,4.831057071685791,-6.898334503173828),12.8605),
			new THREE.Sphere(new THREE.Vector3(3.855361223220825,5.122651100158691,-7.636805057525635),12.8605),
			new THREE.Sphere(new THREE.Vector3(3.137465000152588,19.993364334106445,-7.636802673339844),12.8605)
		];

		var _triangles_per_block = [
			new THREE.Triangle( new THREE.Vector3(2.42044,-0.0,-2.42044),new THREE.Vector3(0.0,-0.0,-3.42302),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(0.0,-0.0,-3.42302),new THREE.Vector3(-2.42044,-0.0,-2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-2.42044,-0.0,-2.42044),new THREE.Vector3(-3.42302,-0.0,0.0),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-3.42302,-0.0,0.0),new THREE.Vector3(-2.42044,-0.0,2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-2.42044,-0.0,2.42044),new THREE.Vector3(-0.0,-0.0,3.42302),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(-0.0,-0.0,3.42302),new THREE.Vector3(2.42044,-0.0,2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(2.42044,-0.0,2.42044),new THREE.Vector3(3.42302,-0.0,0.0),new THREE.Vector3(-0.0,-0.0,-0.0)),
			new THREE.Triangle( new THREE.Vector3(3.42302,-0.0,0.0),new THREE.Vector3(2.42044,-0.0,-2.42044),new THREE.Vector3(-0.0,-0.0,-0.0)),
		];

		window._pos_m2 = [
			new THREE.Vector3(-0.8193731307983398,20.399999618530273,-0.5292025208473206),
			new THREE.Vector3(-4.479830741882324,18.700002670288086,-8.641427040100098),
			new THREE.Vector3(0.6496095061302185,16.999998092651367,-15.914389610290527),
			new THREE.Vector3(9.519840240478516,15.299999237060547,-15.189029693603516),
			new THREE.Vector3(13.399550437927246,13.599998474121094,-7.179350852966309),
			new THREE.Vector3(8.469792366027832,11.899999618530273,0.23041492700576782),
			new THREE.Vector3(-0.4168849587440491,10.200000762939453,-0.25345566868782043),
			new THREE.Vector3(-4.512977600097656,8.5,-8.154668807983398),
			new THREE.Vector3(0.21345621347427368,6.8000006675720215,-15.695758819580078),
			new THREE.Vector3(9.110007286071777,5.100000381469727,-15.453734397888184),
			new THREE.Vector3(13.419450759887695,3.3999993801116943,-7.666829586029053),
			new THREE.Vector3(8.89984130859375,1.7000000476837158,-1.32549786258096*Math.pow(10,-7)),
			new THREE.Vector3(-9.021298552625012*Math.pow(10,-8),6.33073593547806*Math.pow(10,-9),-4.748051907199624*Math.pow(10,-8)),
			new THREE.Vector3(0.0,0.0,-0.0),
			new THREE.Vector3(8.027852058410645,22.10000228881836,0.4371076226234436) 
		];
 
		window._pos_m3 = [
			new THREE.Vector3(8.67996597290039,21.034290313720703,11.03985595703125),
			new THREE.Vector3(23.710304260253906,21.034290313720703,-3.503077507019043),
			new THREE.Vector3(17.816349029541016,21.034290313720703,-21.387920379638672),
			new THREE.Vector3(-0.16066932678222656,21.034290313720703,-26.432785034179688),
			new THREE.Vector3(-13.411100387573242,21.034290313720703,-13.612041473388672),
			new THREE.Vector3(-10.67352294921875,21.034290313720703,4.493243217468262),
			new THREE.Vector3(-5.4067816734313965,7.872863292694092,8.961512565612793),
			new THREE.Vector3(15.497015953063965,7.872863292694092,8.298747062683105),
			new THREE.Vector3(23.156314849853516,7.872863292694092,-8.904203414916992),
			new THREE.Vector3(13.238580703735352,7.872863292694092,-24.723888397216797),
			new THREE.Vector3(-5.189764976501465,7.872863292694092,-24.139610290527344),
			new THREE.Vector3(-15.334038734436035,7.872863292694092,-8.895286560058594),
			new THREE.Vector3(-13.80511474609375,1.5485416650772095,-0.6035787463188171),
			new THREE.Vector3(-10.974553108215332,1.5485416650772095,-18.69455909729004),
			new THREE.Vector3(5.516540050506592,1.5485399961471558,-26.940107345581055),
			new THREE.Vector3(21.14620018005371,1.5485416650772095,-16.725500106811523),
			new THREE.Vector3(21.39229965209961,1.5485416650772095,2.1039199829101562),
			new THREE.Vector3(4.34739875793457,12.110977172851562,-7.064643859863281),
			new THREE.Vector3(4.34739875793457,4.823862075805664,-7.064643859863281),
			new THREE.Vector3(2.6859800815582275,1.5485399961471558,11.457099914550781)  
		];

		Bloques = function(){
		};
		Bloques.prototype = {
			constructor:Bloques, 
			test:function(){
				console.log("hello from Bloques");
			},
			start:function(){  
				var _this = this;
				_loader.load( 
					_models["m2"], 
					function ( geometry, materials ) {  
						for(var _idx in materials){
							materials[_idx].side = THREE.FrontSide;
						}
						var object = new THREE.Mesh( geometry, materials ); 
						for(var _idx in _pos_m2){
							object = object.clone(); 
							object.position.copy(_pos_m2[_idx]);
							object.position.y+=2;
							_this.setCollisionLimits(object);
							_lib_bm2.push(object);
							w_wgl.scene.add( object ); 
						} 
					}
				);
				_loader.load( 
					_models["m3"], 
					function ( geometry, materials ) { 
						for(var _idx in materials){
							materials[_idx].side = THREE.FrontSide;
						} 
						var object = new THREE.Mesh( geometry, materials ); 
						for(var _idx in _pos_m3){
							object = object.clone();
							object.position.copy(_pos_m3[_idx]);
							_this.setCollisionLimits(object);
							_lib_bm3.push(object);
							w_wgl.scene.add( object ); 
						} 
					}
				);

				this.setListener();
			},
			setCollisionLimits:function(block){
				block._lib_triangles = [];
				for(var _idx in _triangles_per_block){
					block._lib_triangles.push(_triangles_per_block[_idx].clone());
				}
				for(var _idx in block._lib_triangles){
					var _triangle = block._lib_triangles[_idx];
					_triangle.a.add(block.position);
					_triangle.b.add(block.position);
					_triangle.c.add(block.position);
					_triangle._lib_lines3 = [
						new THREE.Line3(_triangle.a,_triangle.b),
						new THREE.Line3(_triangle.b,_triangle.c),
						new THREE.Line3(_triangle.c,_triangle.a)
					];
					//el primer segmento de cada triángulo es el que está al borde del bloque.
					_triangle._lib_lines3[0]._lib_vunit = _triangle.b.clone().sub(_triangle.a).normalize();
					_triangle._lib_lines3[1]._lib_vunit = _triangle.c.clone().sub(_triangle.b).normalize();
					_triangle._lib_lines3[2]._lib_vunit = _triangle.a.clone().sub(_triangle.c).normalize();

					var _normal = _triangle._lib_lines3[0]._lib_vunit.clone().cross(_triangle._lib_lines3[1]._lib_vunit).normalize();

					_triangle._lib_plane = new THREE.Plane(
							_normal,
							_normal.dot(_triangle.a)
						); 

					_triangle._lib_plane.uuid = THREE.Math.generateUUID();

					var _middle = this.triangleMiddlePoint(_triangle);
					_triangle.boundingSphere = {
						middle:_middle,
						radius:this.triangleBoundingSphere(_triangle,_middle)
					}; 
				}
				for(var _idx in _lib_spheres_blocks){
					var _sphere = _lib_spheres_blocks[_idx];
					if(!block.geometry.boundingSphere)block.geometry.computeBoundingSphere();
					if(this.sphereMeshCollisionSphere(_sphere,block)){
						if(!_sphere._lib_objs)_sphere._lib_objs = [];
						_sphere._lib_objs.push(block);
					}
				}
			},
			sphereMeshCollisionSphere:function(sphere,mesh){ 
				return mesh.geometry.boundingSphere && sphere.center.distanceTo(mesh.position)<=(sphere.radius+mesh.geometry.boundingSphere.radius);
			},
			triangleMiddlePoint:function(triangle){
				return triangle.a.clone().add(triangle.b).add(triangle.c).multiplyScalar(1/3);
			},
			triangleBoundingSphere:function(triangle,middle){
				var _da = middle.distanceTo(triangle.a);
				var _db = middle.distanceTo(triangle.b);
				var _dc = middle.distanceTo(triangle.c);
				return Math.max(_da,_db,_dc);
			},
			setListener:function(){
			}, 
			listen:function(evt){ 
			}, 
			render:function(){ 
			}, 
			resize:function(){ 
			} 
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Bloques();
					return _instance;
				}
				return null;
			}
		};
	}
)();
