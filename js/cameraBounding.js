var appgame = appgame || {};

appgame.CameraBounding = (
	function(){

		var _instance = null;  

		var _boundingCylinder = {
			radius:0.3,
			minY:-1.5,
			maxY:1.5,
			center:null/*current camera position*/
		};

		var _on_block_top = false;

		var _halfPI = Math.PI/2;

		var _boundingSphere = {
			radius:Math.sqrt(Math.pow(_boundingCylinder.radius,2)+Math.pow(_boundingCylinder.maxY,2)),
			center:null/*current camera position*/
		}; 

		var _tmpv1 = new THREE.Vector3();
		var _tmpv2 = new THREE.Vector3();
		var _tmpv3 = new THREE.Vector3();

		var _up = new THREE.Vector3(0,1,0);

		var _cameraControl = undefined;

		window._debug_console = false;

		CameraBounding = function(camCtrl){
			_cameraControl = camCtrl;
		};
		CameraBounding.prototype = {
			constructor:CameraBounding, 
			test:function(){
				console.log("hello from CameraBounding");
			},
			start:function(){ 
			}, 
			checkCollisions:function(){
				this.checkCollisionCylinderWithBlocks();
			},
			checkCollisionCylinderWithBlocks:function(){
				if(_lib_spheres_blocks && w_wgl && w_wgl.camera){
					var _keys = {};
					var _collide_with_block_obj 		= false;
					var _collide_any_block_hor 			= false;
					var _collide_any_block_spherical 	= false;
					for(var _idx in _lib_spheres_blocks){ 
						var _sphere = _lib_spheres_blocks[_idx];
						if(
							this.intersectionSphereSphere(
								_sphere.center,
								_sphere.radius,
								w_wgl.camera.position,
								_boundingSphere.radius
							)
						){
							if(_sphere._lib_objs){
								for(var _idy in _sphere._lib_objs){
									if(_keys[_idy])continue; 
									_keys[_idy] = 1;
									var _block = _sphere._lib_objs[_idy];
									if(
										this.intersectionSphereSphere(
											_block.position,
											_block._lib_radius,
											w_wgl.camera.position,
											_boundingSphere.radius
										)
									){  
										_collide_any_block_spherical = true;
										//Cmp => Component.
										var _dir =_block.position.clone().sub(w_wgl.camera.position);
										var _dirCmpY = this.obtenerVectorComponente(_dir,_up);
										var _dirCmpXZ = _dir.clone().sub(_dirCmpY);
										if(_dirCmpXZ.length()<=(_boundingCylinder.radius + _block._lib_radius)){ 
											_collide_any_block_hor = true;
											if(_dirCmpY.length()<_boundingCylinder.maxY){ 
												_collide_with_block_obj = true;
												if(_debug_console)console.log(0.1);
												_dirCmpXZ.normalize().applyAxisAngle(_up,_halfPI);
												var _dirPos = w_wgl.camera.position.clone().sub(w_wgl.camera._lib_last_position);
												var _dirPosUnit = _dirPos.clone().normalize();

												//--------helper

												var _diffY = _boundingCylinder.maxY - _dirCmpY.length();
												var _dirPosUnitY = this.obtenerVectorComponente(_dirPosUnit,_up);
												var _dirPosUnitXZ = _dirPosUnit.clone().sub(_dirPosUnitY);

												var _tmpMiddleYPosition = w_wgl.camera.position.clone().sub(this.composeVector3(_dirPosUnitY,_dirPosUnitXZ,_diffY));

												var _dirLast = _block.position.clone().sub(w_wgl.camera._lib_last_position);
												var _dirLastCmpY = this.obtenerVectorComponente(_dirLast,_up);
												var _dirLastCmpXZ = _dirLast.clone().sub(_dirLastCmpY);

												var _diffLastY = _dirLastCmpXZ.length() - (_block._lib_radius + _boundingCylinder.radius);
												var _tmpMiddleXZPosition = w_wgl.camera._lib_last_position.clone().add(this.composeVector3(_dirPosUnitY,_dirPosUnitXZ,_diffLastY));
												
												var _dirMiddleXZ = _block.position.clone().sub(_tmpMiddleXZPosition);
												var _dirMiddleXZCmpY = this.obtenerVectorComponente(_dirMiddleXZ,_up);//requerido
												//var _dirMiddleXZCmpXZ = _dirMiddleXZ.clone().sub(_dirMiddleXZCmpY);

												var _dirMiddleY = _block.position.clone().sub(_tmpMiddleYPosition);
												var _dirMiddleYCmpY = this.obtenerVectorComponente(_dirMiddleY,_up);
												var _dirMiddleYCmpXZ = _dirMiddleY.clone().sub(_dirMiddleYCmpY);//requerido

												var _enable_displacementXZ = true;
												if(_dirMiddleXZCmpY.length()<_boundingCylinder.maxY){  
													if(_dirMiddleYCmpXZ.length()<(_block._lib_radius + _boundingCylinder.radius) ){ 
														//reset jumping 
														if(_debug_console)console.log(1);
														_cameraControl.resetVFrontVRightZero();
														_enable_displacementXZ = false;
														//_cameraControl.setV0VfZero();
														if(w_wgl.camera._lib_last_position.y<_block.position.y){
															//_cameraControl.startGravity();	
														} 
														
													}else{ 
														if(_debug_console)console.log(2);
														//continuar movimiento hacia arriba de salto pero en vertical
														_cameraControl.resetVFrontVRightZero();
														_enable_displacementXZ = false;
													}	
												}else{ 
													if(_dirMiddleYCmpXZ.length()<(_block._lib_radius + _boundingCylinder.radius) ){  
														//bloquear salto e iniciar gravedad a a v0 = 0. 
														if(w_wgl.camera._lib_last_position.y<_block.position.y){
															if(_debug_console)console.log(5.1);  
															_on_block_top = false; 
															/*si la velocidad en Y es menor a cero, entonces dejarlo en 0*/
															_cameraControl.setV0Zero();
														}else{
															if(_debug_console)console.log(5.2);
															_on_block_top = true; 
														}
													}else{
														if(_debug_console)console.log(6);
														_enable_displacementXZ = false;
														_cameraControl.resetVFrontVRightZero();
													}
												}

												//--------end helper 
												if(!_enable_displacementXZ){
													var _dirMov = this.obtenerVectorComponente(_dirPos,_dirCmpXZ);
													var _tmpPosition = _dirMov.add(w_wgl.camera._lib_last_position);
													
													var _tmpDirPos = _tmpPosition.clone().sub(_block.position);
													var _tmpDirPosCmpY = this.obtenerVectorComponente(_tmpDirPos,_up);
													var _tmpDirPosCmpXZ = _tmpDirPos.clone().sub(_tmpDirPosCmpY);
													if(_tmpDirPosCmpXZ.length()<(_boundingCylinder.radius + _block._lib_radius)){
														_tmpDirPosCmpXZ.normalize().multiplyScalar(_boundingCylinder.radius + _block._lib_radius); 
														_tmpPosition.copy(_block.position).add(_tmpDirPosCmpXZ).add(_tmpDirPosCmpY); 
													}  
													w_wgl.camera.position.x = _tmpPosition.x; 
													w_wgl.camera.position.z = _tmpPosition.z;  
												}else{
													w_wgl.camera.position.y = w_wgl.camera._lib_last_position.y;  
												}
											}else{   
												if(_block.position.y>w_wgl.camera.position.y){ 
													if( (_dirCmpY.length() - _boundingCylinder.maxY)<=0 ){

													}
												}
												if(_block.position.y<w_wgl.camera.position.y){ 
												}
											}
										} 
									}
								}
							}
						}
					} 
					if(_collide_any_block_spherical){
						if(_collide_any_block_hor){
							if(_collide_with_block_obj){
								if(_on_block_top){  
									_on_block_top = false; 
									_cameraControl.setAccZero();  
								}else{  
									_cameraControl.resetAcc(); 
								}
							}else{

							}
						}else{
							_cameraControl.resetAcc(); 
						}
					}else{
						_cameraControl.resetAcc();
					}
					/*if(!_collide_with_block_obj && _collide_any_block_hor){
						_cameraControl.resetAcc();  
					}else{  
						if(_collide_with_block_obj){ 
							if(_on_block_top){  
								_on_block_top = false;
								_cameraControl.setAccZero();  
							}else{ 
								_cameraControl.resetAcc(); 
							}
						}
					} */

				}
				window.__on_block_top = _on_block_top;
			}, 
			isOnBlockTop:function(){
				return _on_block_top;
			},
			setOnBlockTop:function(_boolean){
				_on_block_top = _boolean;
			},
			composeVector3:function(v1,v2,scale){
				return new THREE.Vector3(
						v1.x*scale + v2.x * scale,
						v1.y*scale + v2.y * scale,
						v1.z*scale + v2.z * scale
					); 
			},
			obtenerVectorComponente:function(a,b){
				b = b.clone();
				b.normalize();
				var _scalar = a.x*b.x + a.y*b.y+a.z*b.z;
				return b.multiplyScalar(_scalar);
			},
			intersectionSphereSphere:function(s1_center,s1_radius,s2_center,s2_radius){
				return s1_center.distanceTo(s2_center)<=(s1_radius+s2_radius);
			}
		};

		return {
			instance:function(camCtrl){
				if(!_instance){
					_instance = new CameraBounding(camCtrl);
					return _instance;
				}
				return null;
			}
		};
	}
)();
