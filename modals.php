<div class="modals">
	<div id="modal_bienvenida" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog"> 
	    	<!-- Modal content-->
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal">&times;</button>
	        		<h4 class="modal-title">Shooter 3D</h4>
	      		</div>
	      		<div class="modal-body">
	      			<ul class="nav nav-tabs">
				  		<li class="active"><a data-toggle="tab" href="#home">Inicio</a></li>
				  		<li><a data-toggle="tab" href="#menu1">Uso</a></li>
				  		<li><a data-toggle="tab" href="#menu2">Extras</a></li>
					</ul>

					<div class="tab-content">
				  		<div id="home" class="tab-pane fade in active"> 
				  			<br />
			      			<p>
			      				Este aplicativo te permite visualizar objetos 3D(incluso lowpoly) en distintos 
			      				dispositivos. Puedes visualizar los objetos en 360° navegando alrededor de ellos.
			      			</p> 
			        		<div id="ct-1"></div>
			      			<p>
			      				Utiliza las teclas A,D para desplazarte de manera lateral tal como lo hace un cangrejo; 
			      				utiliza las teclas W,S para desplazarte hacia adelante/atrás o para subir/bajar; conmuta 
			      				estas dos últimas opciones utilizando las teclas Q,E. Utiliza la tecla O, presiona y suelta 
			      				y observarás que hace un giro 360° sobre un punto referencial. 
			      			</p>
			        		<div id="ct-2"></div>
			      			<p>
			      				Si te encuentras en un celular o tableta o quizás una laptop con soporte táctil, entonces 
			      				aparecerán unos botones virtuales que podrás usarlo para desplazarte.
			      			</p>
			      			<p>
			      				Si deseas mover la cámara actual, entonces puedes utilizar el botón izquierdo del mouse
			      				para entrar en el modo de bloqueo de pantalla y puedas utilizar el movimiento del ratón 
			      				para mover la visualización de la cámara. Si te encuentras en un dispositivo con soporte táctil, entonces 
			      				tienes que deslizar tu dedo sobre el centro de la pantalla para mover el espacio de visualización de la cámara.
			      			</p>
			      			<p>
			      				<a href="http://arquigames.pe" target="_blank">arquigames</a> Versión 1.1.2
			      			</p>
				  		</div>
				  		<div id="menu1" class="tab-pane fade">
				  			<br />
				    		<div class="media">
						  		<div class="media-left">
						    		<a href="#">
						      			<img class="media-object" src="./imgs/boton_maximize.png" alt="Botón maximizar">
						   		 	</a>
						  		</div>
						  		<div class="media-body">
						  			Habilita/Inhabilita el modo pantalla completa.
						  		</div>
							</div>
				    		<div class="media">
						  		<div class="media-left">
						    		<a href="#">
						      			<img class="media-object" src="./imgs/boton_orbitar_camara.png" alt="Botón orbitar">
						   		 	</a>
						  		</div>
						  		<div class="media-body">
						  			En dipositivos con soporte táctil debes mantenerlo presionado para 
						  			que orbite sobre un punto frontal de referencia.
						  		</div>
							</div> 
							<div>
								<ul>
									<li>Tecla K: Salir del bloqueo de pantalla.</li>
									<li>Teclas A,D: Desplazamiento lateral.</li>
									<li>Teclas W,S: Desplazamiento adelante/atrás ó subir/bajar.</li>
									<li>Teclas Q,E: Conmutan el movimiento adelante/atrás ó subir/bajar. No presionar ambos.</li>
									<li>Teclas O: Presiona y suelta para realizar un giro sobre un punto referencial.</li>
									<li>Mouse: Click sobre la pantalla para entrar en modo bloqueo y 
										controlarás la cámara moviendo el mouse. Si estás en un dispositivo 
										con soporte táctil, esto quizas no funcione y 
										tendrás que deslizar tus dedos o el mouse por la pantalla para
										 simular el movimiento de la cámara.
									</li>
								</ul>
							</div>
				  		</div>
				  		<div id="menu2" class="tab-pane fade">
				  			<br />
				    		<ul> 
				    			<li>Este software está en constante desarrollo por <a href="http://arquigames.pe" target="_blank">arquigames</a> y será utilizado para futuros proyectos 
				    				de videojuegos 3D VR/AR utilizando WebGL.</li>
				    			<li>Este software no está a la venta.</li>
				    		</ul>
				  		</div>
					</div>	
	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	      		</div>
	    	</div> 
	  	</div>
	</div>
</div>